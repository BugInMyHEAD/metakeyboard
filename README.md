# Metakeyboard

Metakeyboard is a highly customizable, easy-to-share IME.  

You can create and modify the layout of the keyboard,
the behavior of its button as you wish.  
Furthermore, you can share your enhanced keyboard layout(XML file) to
help others to type more conveniently on devices with a touch screen.  

## How to use this app

### Ready to use

Install by open the apk file.  

Activate Metakeyboard IME on Android system settings.  
It may differ to find the page the preference exists.  
You may select Metakeyboard to use on a real text box at the nearby preference page
right after activating.  

Open the app drawer, and click the app icon named "Metakeyboard Settings".  
Click "Load Keyboard Layout Directory" to select the directory containing
Metakeyboard layout files(XML files). Storage access permission needed.  

Click "Load Keyboard Layout" to select the keyboard to use. 'Draw over other apps'
permission needed.  
Test the keyboard with the text box appearing by clicking upper right button.  
You can use the keyboard where you need to type.  

### Editing keyboard layout and behavior

Open the app drawer, and click the app icon named "Metakeyboard Layout Editor".  
Click the keyboard layout file to edit or "Empty Keyboard" button.  

You can add or delete keyboard row or button on the page.  
Click "edit button" to edit details of the button.  

You can define the type of the button and add or delete keyboard actions.  

Save by clicking the upper right button.  

### Other settings

Open the app drawer, and click the app icon named "Metakeyboard Settings".  

Keyboard Height:  
It shows keyboard view attached of the size adjusting bar.  
Just touch the screen to resize the view.  
The height will be changed by your finger moving and finished by detaching
your finger from the screen.  

Keyboard Look-and-Feel:  
It shows the page you can designate keyboard view colors and a background image.  
Don't forget setting color transparency preference if you want to see the background image.  

Timer:  
It shows the page related with timing for typing.  
You can adjust the values for your comfort key input.  

## For keyboard layout designers

### Recommendations

- Make a button for switching keyboard by using  
```xml
<call function="showKeyboardList" />
```

## For developers

API docs: https://buginmyhead.gitlab.io/metakeyboard-pages/docs/app/

### How to contribute

#### Coding conventions

Kotlin Coding Conventions(https://kotlinlang.org/docs/reference/coding-conventions.html),
except for indenting with 2 spaces.  
Don't use Unicode on identifiers. Instead, you can use Unicode in comments.  
We strongly recommend you to hard wrap before reach 100 columns in one line.  

### Language Composer

Developers shouldn't forget register their implementation in the file
"findLanguageComposer.kt".  

We recommend you to name composing elements in English and add comment in local
language if it doesn't have only an unique name.  
However, you can contribute even if you couldn't find the proper English name yet. :-)  

Named elements has format like this:  
(1)\_(2)\_(3)\_(4)  
(All letters are small.)  
- (1) Language
- (2) Country
- (3) Type: w(whitespace), h(helper), g(grapheme, would not be necessary)
- (4) Name: name of the element. Including underscore '_'.