package org.metakeyboard.android

import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.kotlintest.shouldBe
import io.kotlintest.specs.FreeSpec
import org.metakeyboard.core.MetakeyboardLayout
import org.metakeyboard.core.PrimitiveComposer
import org.metakeyboard.core.RowBasedKeyboard
import org.metakeyboard.core.rowBasedKeyboard
import org.metakeyboard.languagecomposerimpl.Hangul
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

internal class XmlSerializerTest : FreeSpec() {
  private val file =
    File("./build/outputs/tests").run {
      mkdirs()
      // this xml file will be val `file`
      resolve("XmlSerializerTest.xml")
    }
  private val fileOutputStream = FileOutputStream(file)
  private val fileInputStream = FileInputStream(file)

  init {
    "Basic test" - {
      val basicRowBasedKeyboard = rowBasedKeyboard {
        keyboardRow {
          baseButton {
            shortClickReaction {
              callable(MetakeyboardLayout.Callable.Composee(
                PrimitiveComposer.Category.TEXT, "TEXT", null
              ))
            }
          }
          customLongClickButton {
            hasTimeLimit = true
            longClickReaction {
              callable(MetakeyboardLayout.Callable.Composee(
                PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_add_stroke", Hangul::class
              ))
            }
            shortClickReaction {
              callable(MetakeyboardLayout.Callable.Composee(
                PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class
              ))
            }
            shortClickReaction {
              callable(MetakeyboardLayout.Callable.Composee(
                PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class
              ))
            }
          }
        }
        keyboardRow {
          repeaterButton {}
        }
        keyboardRow {
          weight = 0.5F
        }
      }

      XmlSerializer.serialize(fileOutputStream, basicRowBasedKeyboard)

      val keyboard = XmlDeserializer.rowBasedKeyboard(fileInputStream)

      "keyboard" {
        keyboard.run {
          rows.size shouldBe 3
          orientation shouldBe RowBasedKeyboard.Orientation.HORIZONTAL
          sumRowWeight() shouldBe 2.5F
        }
      }

      "row0" - {
        keyboard.rows[0].run {
          "size" { buttons.size shouldBe 2 }
          "weight" { weight shouldBe 1F }
          "buttons" - {
            "button0" {
              buttons[0].run {
                shouldBeInstanceOf<RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.BaseButton>()
                (this as RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.BaseButton).shortClick.callables.run {
                  size shouldBe 1
                  get(0).shouldBeInstanceOf<MetakeyboardLayout.Callable.Composee>()
                  (get(0) as MetakeyboardLayout.Callable.Composee).composerComposee.run {
                    category shouldBe PrimitiveComposer.Category.TEXT
                    value shouldBe "TEXT"
                    languageComposer shouldBe null
                    print shouldBe null
                  }
                }
              }
            }
            "button1" {
              buttons[1].run {
                shouldBeInstanceOf<RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton>()
                (this as RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton).run {
                  longClick!!.callables.size shouldBe 1
                  longClick!!.callables[0].shouldBeInstanceOf<MetakeyboardLayout.Callable.Composee>()
                  (longClick!!.callables[0] as MetakeyboardLayout.Callable.Composee).composerComposee.run {
                    category shouldBe PrimitiveComposer.Category.NAMED_ELEMENT
                    value shouldBe "ko_kr_h_add_stroke"
                    languageComposer shouldBe Hangul::class
                    print shouldBe null
                  }
                  shortClicks.size shouldBe 2
                  shortClicks[0].callables[0].shouldBeInstanceOf<MetakeyboardLayout.Callable.Composee>()
                  (shortClicks[0].callables[0] as MetakeyboardLayout.Callable.Composee).composerComposee.run {
                    category shouldBe PrimitiveComposer.Category.GRAPHEME
                    value shouldBe "ㄱ"
                    languageComposer shouldBe Hangul::class
                    print shouldBe null
                  }
                  (shortClicks[1].callables[0] as MetakeyboardLayout.Callable.Composee).composerComposee.run {
                    category shouldBe PrimitiveComposer.Category.GRAPHEME
                    value shouldBe "ㄴ"
                    languageComposer shouldBe Hangul::class
                    print shouldBe null
                  }
                }
              }
            }
          }
        }
      }

      "row1" - {
        keyboard.rows[1].run {
          "size" { buttons.size shouldBe 1 }
          "weight" { weight shouldBe 1F }
          "buttons" - {
            "button0" { buttons[0].shouldBeInstanceOf<RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton>() }
          }
        }
      }

      "row2" - {
        keyboard.rows[2].run {
          "size" { buttons.size shouldBe 0 }
          "weight" { weight shouldBe 0.5F }
        }
      }
    }
  }

  override fun afterProject() {
    fileInputStream.close()
    fileOutputStream.close()
    super.afterProject()
  }
}