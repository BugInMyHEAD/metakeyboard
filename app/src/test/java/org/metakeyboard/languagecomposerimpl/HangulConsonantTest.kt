package org.metakeyboard.languagecomposerimpl

import io.kotlintest.shouldBe
import org.metakeyboard.core.LanguageComposerTest
import org.metakeyboard.core.PrimitiveComposer

internal class HangulConsonantTest : LanguageComposerTest(Hangul()) {
  init {
    "ㄱㄱㅆㅆㅎㅎㅅㅅ" {
      composer.run {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㄱ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱ"
        pullUncommitted().toString() shouldBe "ㄱ"
        countConsumed() shouldBe 1
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱㄱ"
        pullUncommitted().toString() shouldBe "ㅆ"
        countConsumed() shouldBe 2
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱㄱㅆ"
        pullUncommitted().toString() shouldBe "ㅆ"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱㄱㅆㅆ"
        pullUncommitted().toString() shouldBe "ㅎ"
        countConsumed() shouldBe 4
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱㄱㅆㅆㅎ"
        pullUncommitted().toString() shouldBe "ㅎ"
        countConsumed() shouldBe 5
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱㄱㅆㅆㅎㅎ"
        pullUncommitted().toString() shouldBe "ㅅ"
        countConsumed() shouldBe 6
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄱㄱㅆㅆㅎㅎㅅ"
        pullUncommitted().toString() shouldBe "ㅅ"
        countConsumed() shouldBe 7
      }
    }

    "ㄻㅄ" {
      composer.run {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㄹ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㄻ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅂ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄻ"
        pullUncommitted().toString() shouldBe "ㅂ"
        countConsumed() shouldBe 2
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class))
        pullCommitted().toString() shouldBe "ㄻ"
        pullUncommitted().toString() shouldBe "ㅄ"
        countConsumed() shouldBe 2
      }
    }

    "ㄱㄴㄷㄹㅁㅂㅅㅇㅈㅊㅋㅌㅍㅎㄲㄸㅃㅆㅉ" {
      composer.run {
        listOf(
          "ㄱ", "ㄴ", "ㄷ", "ㄹ", "ㅁ",
          "ㅂ", "ㅅ", "ㅇ", "ㅈ", "ㅊ",
          "ㅋ", "ㅌ", "ㅍ", "ㅎ", "ㄲ",
          "ㄸ", "ㅃ", "ㅆ", "ㅉ"
        ).forEach {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, it, Hangul::class))
        }
        pullCommitted().toString() shouldBe "ㄱㄴㄷㄻㅄㅇㅈㅊㅋㅌㅍㅎㄲㄸㅃㅆ"
        pullUncommitted().toString() shouldBe "ㅉ"
        countConsumed() shouldBe 18
      }
    }

    "ko_kr_h_add_stroke: ㅋㄷㅌㅂㅍㅈㅊㅎ" {
      composer.run {
        listOf(
          "ㄱ", "ko_kr_h_add_stroke",
          "ㄴ", "ko_kr_h_add_stroke",
          "ㄴ", "ko_kr_h_add_stroke", "ko_kr_h_add_stroke",
          "ㅁ", "ko_kr_h_add_stroke",
          "ㅁ", "ko_kr_h_add_stroke", "ko_kr_h_add_stroke",
          "ㅅ", "ko_kr_h_add_stroke",
          "ㅅ", "ko_kr_h_add_stroke", "ko_kr_h_add_stroke",
          "ㅇ", "ko_kr_h_add_stroke"
        ).forEach {
          if (it == "ko_kr_h_add_stroke")
            push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, it, Hangul::class))
          else
            push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, it, Hangul::class))
        }
        pullCommitted().toString() shouldBe "ㅋㄷㅌㅂㅍㅈㅊ"
        pullUncommitted().toString() shouldBe "ㅎ"
      }
    }

    "ko_kr_h_double_consonant: ㄲㄸㅃㅆㅉ" {
      composer.run {
        listOf(
          "ㄱ", "ko_kr_h_double_consonant",
          "ㄷ", "ko_kr_h_double_consonant",
          "ㅂ", "ko_kr_h_double_consonant",
          "ㅅ", "ko_kr_h_double_consonant",
          "ㅈ", "ko_kr_h_double_consonant"
        ).forEach {
          if (it == "ko_kr_h_double_consonant")
            push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, it, Hangul::class))
          else
            push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, it, Hangul::class))
        }
        pullCommitted().toString() shouldBe "ㄲㄸㅃㅆ"
        pullUncommitted().toString() shouldBe "ㅉ"
      }
    }
  }
}