package org.metakeyboard.languagecomposerimpl

import io.kotlintest.shouldBe
import org.metakeyboard.core.LanguageComposerTest
import org.metakeyboard.core.PrimitiveComposer

internal class HangulVowelTest : LanguageComposerTest(Hangul()) {
  init {
    "ceonjiin" - {
      "vowel: ㅣ ㅏ ㅐ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅣ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅏ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅐ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅑ ㅒ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅑ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅒ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: · ㅓ ㅔ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "·"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅓ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅔ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ‥ ㅕ ㅖ" {
        composer.run {
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_ceonceon",
              Hangul::class
            )
          )
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅖ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅗ ㅚ ㅘ ㅙ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅗ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅚ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅘ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅙ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅛ" {
        composer.run {
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_ceonceon",
              Hangul::class
            )
          )
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅛ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅡ ㅜ ㅟ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅡ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅜ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅟ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅠ ㅝ ㅞ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_ceonceon",
              Hangul::class
            )
          )
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅠ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅝ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅞ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅢ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅢ"
          countConsumed() shouldBe 0
        }
      }
    }

    "narasgeul" - {
      "vowel: ㅏ ㅐ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅏ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅐ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅑ ㅒ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_add_stroke",
              Hangul::class
            )
          )
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅑ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅒ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅡ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅡ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅣ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅣ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅢ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅢ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅗ ㅛ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅗ"
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_add_stroke",
              Hangul::class
            )
          )
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅛ"
          countConsumed() shouldBe 0
        }
      }
      "vowel: ㅜ ㅠ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅜ"
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_add_stroke",
              Hangul::class
            )
          )
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅠ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅘ ㅙ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅘ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅙ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅝ ㅞ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅝ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅞ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅚ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅚ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅟ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅟ"
          countConsumed() shouldBe 0
        }
      }
    }

    "vega" - {
      "vowel: ㅑ ㅒ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅑ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅑ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅒ"
          countConsumed() shouldBe 0
        }
      }

      "vowel: ㅕ ㅖ" {
        composer.run {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅕ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅕ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅖ"
          countConsumed() shouldBe 0
        }
      }
    }
  }
}