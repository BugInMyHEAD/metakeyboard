package org.metakeyboard.languagecomposerimpl

import io.kotlintest.shouldBe
import org.metakeyboard.core.LanguageComposerTest
import org.metakeyboard.core.PrimitiveComposer

internal class HangulWordTest : LanguageComposerTest(Hangul()) {
  init {
    composer.run {
      "눼" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "눼"
      }

      "왰 - 3중 모음" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "왰"
      }

      "궤양 - 2중 모음" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅔ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅑ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        pullCommitted().toString() shouldBe "궤"
        pullUncommitted().toString() shouldBe "양"
        countConsumed() shouldBe 3
      }

      "왰 - 2중 모음" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅐ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "왰"
      }

      "궤양 - 3중 모음" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅜ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅑ", Hangul::class))
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        pullCommitted().toString() shouldBe "궤"
        pullUncommitted().toString() shouldBe "양"
      }

      "기지 (check composing choseong that can be a jongsung)" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㄱ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "기"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅈ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "깆"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        pullCommitted().toString() shouldBe "기"
        pullUncommitted().toString() shouldBe "지"
        countConsumed() shouldBe 2
      }

      "기쁨 (check composing choseong that can't be a jongsung)" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㄱ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "기"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅃ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "기ㅃ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
        pullCommitted().toString() shouldBe "기"
        pullUncommitted().toString() shouldBe "쁘"
        countConsumed() shouldBe 2
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class))
        pullCommitted().toString() shouldBe "기"
        pullUncommitted().toString() shouldBe "쁨"
        countConsumed() shouldBe 2
      }

      "check jongseong composing when fixed and floating jongseong can be combined" - {
        "길항" {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㄱ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "기"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "길"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "긿"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe "길"
          pullUncommitted().toString() shouldBe "하"
          countConsumed() shouldBe 3
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
          pullCommitted().toString() shouldBe "길"
          pullUncommitted().toString() shouldBe "항"
          countConsumed() shouldBe 3
        }

        "넉살" {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㄴ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "너"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "넉"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "넋"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe "넉"
          pullUncommitted().toString() shouldBe "시"
          countConsumed() shouldBe 3
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_ceon",
              Hangul::class,
              "·"
            )
          )
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄹ", Hangul::class))
          pullCommitted().toString() shouldBe "넉"
          pullUncommitted().toString() shouldBe "살"
          countConsumed() shouldBe 3
        }

        "환호" {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅎ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "호"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "화"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "환"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "홚"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅗ", Hangul::class))
          pullCommitted().toString() shouldBe "환"
          pullUncommitted().toString() shouldBe "호"
          countConsumed() shouldBe 4
        }
      }

      "암막 (check floating jongseong composing when jongseong equals to choseong)" - {
        "with ko_kr_h_ceon" {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅇ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "이"
          countConsumed() shouldBe 0
          push(
            PrimitiveComposer.Composee(
              PrimitiveComposer.Category.NAMED_ELEMENT,
              "ko_kr_h_ceon",
              Hangul::class,
              "·"
            )
          )
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "아"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "암"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "암ㅁ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
          pullCommitted().toString() shouldBe "암"
          pullUncommitted().toString() shouldBe "미"
          countConsumed() shouldBe 4
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.NAMED_ELEMENT, "ko_kr_h_ceon", Hangul::class))
          pullCommitted().toString() shouldBe "암"
          pullUncommitted().toString() shouldBe "마"
          countConsumed() shouldBe 4
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
          pullCommitted().toString() shouldBe "암"
          pullUncommitted().toString() shouldBe "막"
          countConsumed() shouldBe 4
        }

        "without ko_kr_h_ceon" {
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "ㅇ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "아"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "암"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class))
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe "암ㅁ"
          countConsumed() shouldBe 0
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
          pullCommitted().toString() shouldBe "암"
          pullUncommitted().toString() shouldBe "마"
          countConsumed() shouldBe 3
          push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄱ", Hangul::class))
          pullCommitted().toString() shouldBe "암"
          pullUncommitted().toString() shouldBe "막"
          countConsumed() shouldBe 3
        }
      }

      "아싸 (check floating jongseong composing without fixed jongseong)" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㅇ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "아"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅆ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "았"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
        pullCommitted().toString() shouldBe "아"
        pullUncommitted().toString() shouldBe "싸"
        countConsumed() shouldBe 2
      }

      "안녕하 (check floating jongseong composing with fixed jongseong)" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㅇ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "아"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "안"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "안ㄴ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
        pullCommitted().toString() shouldBe "안"
        pullUncommitted().toString() shouldBe "너"
        countConsumed() shouldBe 3
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_add_stroke",
            Hangul::class
          )
        )
        pullCommitted().toString() shouldBe "안"
        pullUncommitted().toString() shouldBe "녀"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        pullCommitted().toString() shouldBe "안"
        pullUncommitted().toString() shouldBe "녕"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        pullCommitted().toString() shouldBe "안"
        pullUncommitted().toString() shouldBe "녕ㅇ"
        countConsumed() shouldBe 3
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_add_stroke",
            Hangul::class
          )
        )
        pullCommitted().toString() shouldBe "안"
        pullUncommitted().toString() shouldBe "녕ㅎ"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅏ", Hangul::class))
        pullCommitted().toString() shouldBe "안녕"
        pullUncommitted().toString() shouldBe "하"
        countConsumed() shouldBe 7
      }

      "하세요 (check pending jungseong composing)" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㅎ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "히"
        countConsumed() shouldBe 0
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_ceon",
            Hangul::class,
            "·"
          )
        )
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "하"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅅ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "핫"
        countConsumed() shouldBe 0
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_ceon",
            Hangul::class,
            "·"
          )
        )
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "핫·"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        pullCommitted().toString() shouldBe "하"
        pullUncommitted().toString() shouldBe "서"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅣ", Hangul::class))
        pullCommitted().toString() shouldBe "하"
        pullUncommitted().toString() shouldBe "세"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅇ", Hangul::class))
        pullCommitted().toString() shouldBe "하"
        pullUncommitted().toString() shouldBe "셍"
        countConsumed() shouldBe 3
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_ceonceon",
            Hangul::class,
            ":"
          )
        )
        pullCommitted().toString() shouldBe "하"
        pullUncommitted().toString() shouldBe "셍:"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
        pullCommitted().toString() shouldBe "하세"
        pullUncommitted().toString() shouldBe "요"
        countConsumed() shouldBe 7
      }

      "버튼 (check composing ko_kr_h_add_stroke)" {
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅁ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㅁ"
        countConsumed() shouldBe 0
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_add_stroke",
            Hangul::class
          )
        )
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "ㅂ"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅓ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "버"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "번"
        countConsumed() shouldBe 0
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_add_stroke",
            Hangul::class
          )
        )
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "벋"
        countConsumed() shouldBe 0
        push(
          PrimitiveComposer.Composee(
            PrimitiveComposer.Category.NAMED_ELEMENT,
            "ko_kr_h_add_stroke",
            Hangul::class
          )
        )
        pullCommitted().toString() shouldBe ""
        pullUncommitted().toString() shouldBe "벝"
        countConsumed() shouldBe 0
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅡ", Hangul::class))
        pullCommitted().toString() shouldBe "버"
        pullUncommitted().toString() shouldBe "트"
        countConsumed() shouldBe 3
        push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㄴ", Hangul::class))
        pullCommitted().toString() shouldBe "버"
        pullUncommitted().toString() shouldBe "튼"
        countConsumed() shouldBe 3
      }
    }
  }
}