package org.metakeyboard.integrationcomposerimpl

import io.kotlintest.shouldBe
import org.metakeyboard.core.IntegrationComposerTest
import org.metakeyboard.core.PrimitiveComposer
import org.metakeyboard.languagecomposerimpl.Hangul

internal class OfficialIntegrationComposerTest :
  IntegrationComposerTest(MetakeyboardOfficialIntegrationComposer()) {
  init {
    "한글English한글" - {
      fun pushHangul(volatile: Boolean) {
        arrayOf("ㅎ", "ㅏ", "ㄴ", "ㄱ", "ㅡ", "ㄹ").forEach {
          composer.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, it, Hangul::class), volatile)
        }
      }

      fun pushEnglish(volatile: Boolean) {
        arrayOf("Eng", "l", "is", "h")
          .forEach {
            composer.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.TEXT, it, null), volatile)
          }
      }

      "'stable'" {
        pushHangul(false)
        composer.pullCommitted().toString() shouldBe "한"
        composer.pullUncommitted().toString() shouldBe "글"
        pushEnglish(false)
        composer.pullCommitted().toString() shouldBe "한글English"
        composer.pullUncommitted().toString() shouldBe ""
        pushHangul(false)
        composer.pullCommitted().toString() shouldBe "한글English한"
        composer.pullUncommitted().toString() shouldBe "글"
      }

      "'volatile'" {
        pushHangul(true)
        composer.pullCommitted().toString() shouldBe ""
        composer.pullUncommitted().toString() shouldBe "한글"
        pushEnglish(true)
        composer.pullCommitted().toString() shouldBe ""
        composer.pullUncommitted().toString() shouldBe "한글English"
        pushHangul(true)
        composer.pullCommitted().toString() shouldBe ""
        composer.pullUncommitted().toString() shouldBe "한글English한글"
      }
    }

    "ㅎㅎㅎ " {
      composer.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      composer.pullCommitted().toString() shouldBe ""
      composer.pullUncommitted().toString() shouldBe "ㅎ"
      composer.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      composer.pullCommitted().toString() shouldBe "ㅎ"
      composer.pullUncommitted().toString() shouldBe "ㅎ"
      composer.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.GRAPHEME, "ㅎ", Hangul::class))
      composer.pullCommitted().toString() shouldBe "ㅎㅎ"
      composer.pullUncommitted().toString() shouldBe "ㅎ"
      composer.push(PrimitiveComposer.Composee(PrimitiveComposer.Category.TEXT, " ", null))
      composer.pullCommitted().toString() shouldBe description().name
      composer.pullUncommitted().toString() shouldBe ""
    }
  }
}