package org.metakeyboard.core

import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.extensions.TestListener
import io.kotlintest.shouldBe
import io.kotlintest.specs.FreeSpec

internal abstract class PrimitiveComposerTest(
  protected open val composer: PrimitiveComposer
) : FreeSpec() {
  protected open val listOfTestListener: List<TestListener> = listOf(
    object : TestListener {
      override fun afterTest(testCase: TestCase, result: TestResult) {
        composer.run {
          clearCommitted()
          clearUncommitted()
          pullCommitted().toString() shouldBe ""
          pullUncommitted().toString() shouldBe ""
        }
      }
    }
  )

  override fun listeners(): List<TestListener> = listOfTestListener

  init {
    "'PrimitiveComposer initial state'" {
      composer.pullCommitted().toString() shouldBe ""
      composer.pullUncommitted().toString() shouldBe ""
    }
  }
}

internal abstract class LanguageComposerTest(
  override val composer: LanguageComposer
) : PrimitiveComposerTest(composer) {
  override val listOfTestListener: List<TestListener> =
    listOf(
      object : TestListener {
        override fun afterTest(testCase: TestCase, result: TestResult) {
          composer.countConsumed() shouldBe 0
        }
      }
    ) + super.listOfTestListener

  init {
    "'LanguageComposer initial state'" {
      composer.countConsumed() shouldBe 0
    }
  }
}

internal abstract class IntegrationComposerTest(
  override val composer: IntegrationComposer
) : PrimitiveComposerTest(composer)