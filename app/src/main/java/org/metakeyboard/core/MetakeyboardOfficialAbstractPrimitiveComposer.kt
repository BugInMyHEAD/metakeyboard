package org.metakeyboard.core

import java.util.*

/**
 * When a user [push]es [PrimitiveComposer.Composee]s, it just holds them.
 * When a user invokes [pullCommitted] or [pullUncommitted], it starts to [compose] if
 * [stableComposees] content has been changed by [push]ing.
 */
abstract class MetakeyboardOfficialAbstractPrimitiveComposer : PrimitiveComposer {
  /**
   * It stores the [push]ed [PrimitiveComposer.Composee]s.
   */
  protected val stableComposees = LinkedList<PrimitiveComposer.Composee>()
  /**
   * It stores composed uncommitted [CharSequence] used by the [pullUncommitted] method.
   */
  protected val uncommittedBuilder = StringBuilder()
  /**
   * It stores composed committed [CharSequence] used by the [pullCommitted] method.
   */
  protected val committedBuilder = StringBuilder()
  /**
   * It is an [Boolean] that means stored [PrimitiveComposer.Composee] sequence has been
   * changed by [push].
   */
  private var isDirty = true

  final override fun pullCommitted(): CharSequence {
    needToCompose()
    return committedBuilder
  }

  final override fun pullUncommitted(): CharSequence {
    needToCompose()
    return uncommittedBuilder
  }

  override fun clearCommitted() {
    committedBuilder.clear()
  }

  /**
   * It [compose] if a user needs updated composed [CharSequence].
   *
   * @see [isDirty]
   */
  protected fun needToCompose() {
    if (!isDirty) return
    compose()
    isDirty = false
  }

  /**
   * Main composing algorithm.
   *
   * With data stored in this instance(e.g. [stableComposees]), you must provide a way to compose,
   * and [StringBuilder.append] committed and uncommitted [CharSequence] at
   * [committedBuilder] or [uncommittedBuilder].
   */
  protected abstract fun compose()

  /**
   * It is used by child classes in this package to make [isDirty] true.
   */
  internal fun makeDirty() {
    isDirty = true
  }
}