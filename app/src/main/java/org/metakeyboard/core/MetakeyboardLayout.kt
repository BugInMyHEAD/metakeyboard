package org.metakeyboard.core

import kotlin.reflect.KClass

/**
 * Representation of Metakeyboard layout.
 *
 * An instance holds list of keyboards in one Metakeyboard layout.
 *
 * This class has a variety of essential classes and interfaces to implement a keyboard class.
 */
class MetakeyboardLayout(
  val initialSetting: Action,
    //val onSwitch = Action(),
  _keyboards: List<Keyboard>
) {
  /**
   * List of keyboards in this layout.
   */
  val keyboards = _keyboards.toList()

  /**
   * Unit function keyboards can do.
   */
  sealed class Callable {
    /**
     * It does what a user configured.
     */
    abstract fun call()

    /**
     * Wrapper of [MetakeyboardModel.Callee]; special keyboard function.
     */
    data class Callee(
      val callee: MetakeyboardModel.Callee
    ) : Callable() {
      constructor(
        functionName: String,
        arguments: List<MetakeyboardModel.Callee.Argument>
      ) : this(MetakeyboardModel.Callee(functionName, arguments))

      override fun call() {
        MetakeyboardModel.call(callee)
      }
    }

    /**
     * Wrapper of [PrimitiveComposer.Composee].
     */
    data class Composee(
      val composerComposee: PrimitiveComposer.Composee
    ) : Callable() {
      constructor(
        category: PrimitiveComposer.Category,
        value: String,
        languageComposer: KClass<out LanguageComposer>?,
        print: String? = null
      ) : this(PrimitiveComposer.Composee(category, value, languageComposer, print))

      /**
       * It [IntegrationComposer.push]es [composerComposee] into
       * [MetakeyboardModel.integrationComposer] with volatility.
       */
      override fun call() {
        MetakeyboardModel.integrationComposer.push(composerComposee, true)
      }
    }
  }

  /**
   * List of [Callable] [Callable.call]ed when a keyboard button is clicked.
   */
  open class Action(
    _callables: List<Callable>
  ) {
    /**
     * List of [Callable].
     */
    val callables = _callables.toList()

    /**
     * [Callable.call] all [Callable]s in order.
     */
    fun act() = callables.forEach { it.call() }

    open fun toBuilder(): Builder = toBuilderProtected(Builder())

    protected fun toBuilderProtected(builder: Builder): Builder {
      builder.callables.addAll(callables)
      return builder
    }

    open class Builder {
      var callables = mutableListOf<Callable>()

      open fun build() = Action(callables)

      fun callable(c: Callable) = callables.add(c)
    }
  }

  /**
   * Representation of keyboard behavior that have to be implemented by keyboard classes.
   */
  interface ModelDelegate {
    /**
     * It receives touch events from the keyboard view.
     */
    fun onTouch(x: Float, y: Float, touchAction: TouchAction)
    /**
     * It receives timer events.
     */
    fun onTime(timerEvent: TimerEvent)

    /**
     * Kind of touch action.
     */
    enum class TouchAction {
      /**
       * Touch down.
       */
      DOWN,
      /**
       * Touch coordinate moves after touching down.
       */
      MOVE,
      /**
       *  Touch up.
       */
      UP
    }
    /**
     * Kind of timer event.
     */
    enum class TimerEvent {
      /**
       * It means the keyboard user touches the keyboard button long enough.
       */
      LONG_CLICK,
      /**
       * It means certain amount of time has gone from the keyboard user clicked
       * the keyboard button at the last.
       */
      SLOW_CLICK,
      /**
       * It means auto-repeat has worked.
       */
      REPEAT
    }
  }

  /**
   * Representation of keyboard.
   */
  abstract class Keyboard(
    /**
     * [Action.act] on switching keyboard in this Metakeyboard layout.
     */
    val initialSetting: Action
  ) {
//    fun getModelDelegate(width: Int, height: Int): ModelDelegate {
//      require(width >= 0) { "Width must be non-negative, was $width" }
//      require(height >= 0) { "Height must be non-negative, was $height" }
//      return makeModelDelegate(width, height)
//    }
//
//    protected abstract fun makeModelDelegate(width: Int, height: Int): ModelDelegate
  }

  /**
   * Representation of keyboard key also called button.
   */
  abstract class Button
}