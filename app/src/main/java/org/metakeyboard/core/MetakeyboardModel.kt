package org.metakeyboard.core

import org.metakeyboard.core.MetakeyboardModel.Callee.Argument.Type
import org.metakeyboard.integrationcomposerimpl.MetakeyboardOfficialIntegrationComposer
import kotlin.reflect.KClass
import kotlin.reflect.KVisibility
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.valueParameters

/**
 * The singleton object that interacts with the main [IntegrationComposer] and the OS, and
 * stores user-defined variables.
 *
 * All functors named '...Callback' are OS dependent, so their implementation lie on
 * application-system layer developers.
 */
object MetakeyboardModel {
  /**
   * Main [IntegrationComposer]. All [PrimitiveComposer.Composee]s are pushed into this by
   * [MetakeyboardLayout.Callable.Composee.call].
   */
  var integrationComposer: IntegrationComposer = MetakeyboardOfficialIntegrationComposer()

  /**
   * A functor to show keyboard list on request.
   */
  var showKeyboardListCallback: () -> Unit = {}
  /**
   * A functor to open another app on request.
   */
  var openAppCallback: (List<String>) -> Unit = { _: List<String> -> }
  /**
   * A functor to open Metakeyboard settings on request.
   */
  var openSettingsCallback: () -> Unit = {}
  /**
   * A functor to resize keyboard view on request.
   */
  var resizeCallback: () -> Unit = {}

  /**
   * User-defined variables by [setVariable].
   */
  private val variables =
    mutableMapOf<String, String>()
  /**
   * User-defined options for specific [LanguageComposer]s set by
   * [setLanguageComposerOption].
   */
  private val languageComposerOptions =
    mutableMapOf<KClass<out LanguageComposer>, MutableMap<String, String>>()
  /**
   * User-defined options for specific [IntegrationComposer]s set by
   * [setIntegrationComposerOption].
   */
  private val integrationComposerOptions =
    mutableMapOf<KClass<out IntegrationComposer>, MutableMap<String, String>>()

  /**
   * The [Map] to designate a special keyboard function in this class.
   *
   * @sample call
   */
  private val keyboardFunctions =
    // For public member functions which can be used as a keyboard function
    this::class.memberFunctions.filter { kFunction ->
      kFunction.visibility == KVisibility.PUBLIC
          && kFunction.returnType.toString() == Unit.toString()
          && kFunction.valueParameters.size in 0 .. 1
    }.associate { kFunction ->
      when (kFunction.valueParameters.map { kParameter -> kParameter.type.toString() }) {
        listOf("kotlin.collections.List<kotlin.String>") ->
          Pair(kFunction.name, { args: List<String> -> kFunction.call(this, args); Unit })
        listOf<String>() ->
          Pair(kFunction.name, { _: List<String> -> kFunction.call(this); Unit })
        else ->
          Pair("", { _: List<String> -> Unit })
      }
    } + // For public member properties named "...Callback",
        // which can be used as a keyboard function
        this::class.memberProperties.filter {
          it.visibility == KVisibility.PUBLIC
              && it.name.endsWith("Callback")
        }.associate {
          val suffixRemovedName = it.name.removeSuffix("Callback")
          @Suppress("UNCHECKED_CAST")
          when (it.returnType.toString()) {
            "(kotlin.collections.List<kotlin.String>) -> kotlin.Unit" -> Pair(
              suffixRemovedName,
              { args: List<String> -> (it.call(this) as (List<String>) -> Unit).invoke(args) }
            )
            "() -> kotlin.Unit" -> Pair(
              suffixRemovedName,
              { _: List<String> -> (it.call(this) as () -> Unit).invoke() }
            )
            else -> throw NoWhenBranchMatchedException()
          }
        }

  /**
   * Called by [MetakeyboardLayout.Callable.Callee.call], and then invoke a special keyboard
   * function in this class.
   */
  fun call(callee: Callee) {
    callee.arguments.map {
      when (it.type) {
        Callee.Argument.Type.RAW -> it.value
        Callee.Argument.Type.MAP -> variables[it.value]!!
      }
    }.also {
      keyboardFunctions[callee.functionName]?.invoke(it)
    }
  }

  /**
   * It sets an option variable for specific [IntegrationComposer].
   *
   * @see setLanguageComposerOption
   */
  @Suppress("UNUSED")
  fun setIntegrationComposerOption(args: List<String>) {
    integrationComposerOptions
      // TODO: consider if there will be another integration composer
      .getOrPut(MetakeyboardOfficialIntegrationComposer::class, ::mutableMapOf)[args[1]] = args[2]
  }

  /**
   * It sets an option variable for specific [LanguageComposer].
   *
   * @see setIntegrationComposerOption
   */
  @Suppress("UNUSED")
  fun setLanguageComposerOption(args: List<String>) {
    languageComposerOptions
      .getOrPut(findLanguageComposer(args[0]), ::mutableMapOf)[args[1]] = args[2]
  }

  /**
   * It sets a user-defined variable.
   */
  @Suppress("UNUSED")
  fun setVariable(args: List<String>) {
    variables[args[0]] = args[1]
  }

  /**
   * Representation of a special keyboard function call to this [MetakeyboardModel] by a user.
   */
  class Callee(
    /**
     * Name for the special keyboard function to call.
     */
    val functionName: String,
    /**
     * Argument list for the call which will be copied for keeping immutable.
     */
    _arguments: List<Argument>
  ) {
    /**
     * Argument list for the call.
     */
    val arguments = _arguments.toList()

    /**
     * Representation of an argument of a special keyboard function call.
     */
    data class Argument(
      /**
       * The [Type] to use [value] properly.
       *
       * @see [Type]
       */
      val type: Type,
      /**
       * The value of this [Argument].
       */
      val value: String
    ) {
      /**
       * It determines how to use [value] at [call].
       */
      enum class Type {
        /**
         * It means [value] is the actual value that the user wants.
         */
        RAW,
        /**
         * It means the actual value is in a user-defined variable whose name is [value].
         */
        MAP
      }
    }
  }
}