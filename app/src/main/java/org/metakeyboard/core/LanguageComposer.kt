package org.metakeyboard.core

/**
 * It provides a way to composing [PrimitiveComposer.Composee]s in a local language composing
 * automaton.
 */
interface LanguageComposer : PrimitiveComposer {
  /**
   * The number of [PrimitiveComposer.Composee]s poped by composing committed [CharSequence],
   * which are no more necessary to hold.
   *
   * @return [Int] equal to or more than 0.
   */
  fun countConsumed(): Int
}