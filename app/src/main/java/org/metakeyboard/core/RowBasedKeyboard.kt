package org.metakeyboard.core

import org.metakeyboard.common.NonNegativeFloat
import org.metakeyboard.common.cumulate
import org.metakeyboard.core.RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Rate
import org.metakeyboard.core.RowBasedKeyboard.AbstractButton.Style
import org.metakeyboard.core.RowBasedKeyboard.ButtonText.Text

/**
 * It builds [RowBasedKeyboard] with Kotlin DSL.
 *
 * @return [RowBasedKeyboard] that [init] applied.
 */
fun rowBasedKeyboard(init: RowBasedKeyboard.Builder.() -> Unit) =
  RowBasedKeyboard.Builder().also(init).build()

/**
 * Representation of row-based keyboard.
 */
class RowBasedKeyboard(
  /**
   * Orientation of all keyboard rows.
   */
  val orientation: Orientation,
  initialSetting: MetakeyboardLayout.Action,
  rows: List<KeyboardRow>
) : MetakeyboardLayout.Keyboard(initialSetting) {
  /**
   * Keyboard row list.
   */
  val rows = rows.toList()

  fun toBuilder() = Builder().also { builder ->
    builder.orientation = orientation
    builder.initialSetting = initialSetting
    rows.mapTo(builder.rows) { it.toBuilder() }
  }

  class Builder {
    var orientation: Orientation = Orientation.HORIZONTAL
    var initialSetting: MetakeyboardLayout.Action = MetakeyboardLayout.Action(listOf())
    var rows = mutableListOf<KeyboardRow.Builder>()

    fun build() = RowBasedKeyboard(orientation, initialSetting, rows.map { it.build() })

    fun keyboardRow(init: KeyboardRow.Builder.() -> Unit) =
      rows.add(KeyboardRow.Builder().also(init))
  }

  /**
   * Text imprinted on keyboard button.
   */
  class ButtonText(
    /**
     * List of [Text]
     */
    val texts: List<Text>,
    val firstPadding: Int = 0,
    val secondPadding: Int = 0,
    val thirdPadding: Int = 0,
    val fourthPadding: Int = 0,
    val lineGravity: Float = 0F,
    val textGravity: Float = 0F,
    val lineOffset: Int = 0,
    val textOffset: Int = 0,
    val lineAlignment: Float = 0F,
    val textAlignment: Float = 0F
  ) {
    /**
     * Text segment in sentence of [ButtonText].
     */
    class Text(
      /**
       * Content of this text segment.
       */
      val text: String,
      fontSize: Float = 1F
    ) {
      /**
       * Font size of this text segment.
       */
      val fontSize by NonNegativeFloat(fontSize)

      class Builder {
        var text: String = ""
        var fontSize = 1F

        fun build() = Text(text, fontSize)
      }
    }

    class Builder {
      var texts = mutableListOf<Text>()
      var firstPadding: Int = 0
      var secondPadding: Int = 0
      var thirdPadding: Int = 0
      var fourthPadding: Int = 0
      var lineGravity: Float = 0F
      var textGravity: Float = 0F
      var lineOffset: Int = 0
      var textOffset: Int = 0
      var lineAlignment: Float = 0F
      var textAlignment: Float = 0F

      fun build() = ButtonText(
        texts,
        firstPadding, secondPadding, thirdPadding, fourthPadding,
        lineGravity, textGravity,
        lineOffset, textOffset,
        lineAlignment, textAlignment
      )

      fun text(init: Text.Builder.() -> Unit) =
        texts.add(Text.Builder().also(init).build())
    }
  }

  class Reaction(
    imprints: List<ButtonText>,
    callables: List<MetakeyboardLayout.Callable>
  ) : MetakeyboardLayout.Action(callables) {
    val imprints = imprints.toList()

    fun imprint() {
      TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun toBuilder(): Builder = toBuilderProtected(Builder())

    private fun toBuilderProtected(builder: Builder): Builder {
      super.toBuilderProtected(builder)
      builder.imprints.addAll(imprints)
      return builder
    }

    class Builder : MetakeyboardLayout.Action.Builder() {
      var imprints = mutableListOf<ButtonText>()

      override fun build() = Reaction(imprints, callables)

      fun buttonText(init: ButtonText.Builder.() -> Unit) =
        imprints.add(ButtonText.Builder().also(init).build())
    }
  }

  /**
   * Val [weight] owner.
   */
  interface Weighable {
    /**
     * It is used to calculate ratio of a specific object's [weight] to
     * summation of all related objects' [weight].
     */
    val weight: Float
  }

  /**
   * Representation of button of row-based keyboard.
   */
  sealed class AbstractButton(
    /**
     * [Style] determines how this button looks.
     */
    val style: Style,
    weight: Float,
    val imprints: List<ButtonText>
  ) : MetakeyboardLayout.Button(), Weighable {
    final override val weight by NonNegativeFloat(weight)

    abstract fun toBuilder(): Builder

    protected fun toBuilderProtected(builder: Builder) {
      builder.style = style
      builder.weight = weight
      builder.imprints.addAll(imprints)
    }

    /**
     * Val [longClick] owner.
     */
    interface LongClickable {
      /**
       * [Reaction] that [Reaction.act]s when a user long-clicks the button.
       * Ignore if it is null.
       */
      val longClick: Reaction?
    }

    interface LongClickableBuilder {
      var longClick: Reaction.Builder?

      fun longClickReaction(init: Reaction.Builder.() -> Unit) {
        longClick = Reaction.Builder().also(init)
      }
    }

    /**
     * It determines how the button looks.
     */
    enum class Style { EMPTY_SPACE, NORMAL }

    abstract class Builder {
      var style = Style.NORMAL
      var weight = 1F
      var imprints = mutableListOf<ButtonText>()

      abstract fun build(): AbstractButton

      fun buttonText(init: ButtonText.Builder.() -> Unit) =
        imprints.add(ButtonText.Builder().also(init).build())

      abstract fun shortClickReaction(init: Reaction.Builder.() -> Unit)
    }

    /**
     * [AbstractButton] which has only one [shortClick] [Reaction].
     */
    sealed class AbstractNonSwitcherButton(
      style: Style,
      weight: Float,
      imprints: List<ButtonText>,
      /**
       * [Reaction] that [Reaction.act]s when a user short-clicks the button.
       */
      val shortClick: Reaction
    ) : AbstractButton(style, weight, imprints) {
      protected fun toBuilderProtected(builder: Builder): Builder {
        super.toBuilderProtected(builder)
        builder.shortClick = shortClick.toBuilder()
        return builder
      }

      abstract class Builder : AbstractButton.Builder() {
        var shortClick = Reaction.Builder().also {
          it.imprints = mutableListOf()
          it.callables = mutableListOf()
        }

        final override fun shortClickReaction(init: Reaction.Builder.() -> Unit) {
          shortClick = Reaction.Builder().also(init)
        }
      }

      /**
       * [AbstractNonSwitcherButton] which a user can short-click only.
       */
      class BaseButton(
        style: Style,
        weight: Float,
        imprints: List<ButtonText>,
        shortClick: Reaction
      ) : AbstractNonSwitcherButton(
        style, weight, imprints, shortClick
      ), PopupRow.Popable {
        override fun toBuilder(): Builder = toBuilderProtected(
          Builder()
        )

        private fun toBuilderProtected(builder: Builder): Builder {
          super.toBuilderProtected(builder)
          return builder
        }

        class Builder : AbstractNonSwitcherButton.Builder() {
          override fun build() =
            BaseButton(style, weight, imprints, shortClick.build())
        }
      }

      /**
       * [AbstractNonSwitcherButton] which a user can short-click, and long-click with
       * automatic [Reaction] repeats. It repeats [shortClick] if [longClick] is null,
       * repeats [longClick] otherwise.
       */
      class RepeaterButton(
        style: Style,
        weight: Float,
        imprints: List<ButtonText>,
        override val longClick: Reaction?,
        shortClick: Reaction,
        /**
         * [Rate] of this [RepeaterButton] a user expects.
         */
        val rate: Rate = Rate.FAST
      ) : AbstractNonSwitcherButton(
        style, weight, imprints, shortClick
      ), LongClickable {
        override fun toBuilder() = toBuilderProtected(Builder())

        private fun toBuilderProtected(builder: Builder): Builder {
          super.toBuilderProtected(builder)
          builder.longClick = longClick?.toBuilder()
          builder.rate = rate
          return builder
        }

        /**
         * Rate of repetition. Actual timing depends on user settings.
         */
        enum class Rate {
          FAST, SLOW, ACCELERATE
        }

        class Builder : AbstractNonSwitcherButton.Builder(),
          LongClickableBuilder {
          override var longClick: Reaction.Builder? = null
          var rate: Rate =
            Rate.FAST

          override fun build() = RepeaterButton(
            style, weight, imprints, longClick?.build(), shortClick.build(), rate
          )
        }
      }
    }

    /**
     * [AbstractButton] which has multiple [shortClicks] [Reaction]s. A user can switch composing
     * action with multiple clicks.
     */
    sealed class AbstractSwitcherButton(
      style: Style,
      weight: Float,
      imprints: List<ButtonText>,
      shortClicks: List<Reaction>,
      /**
       * Force the keyboard user to click fast to switch a composing action.
       */
      val hasTimeLimit: Boolean = false,
      /**
       * No more switching at the end of [shortClicks] [Reaction] list.
       */
      val isOneWay: Boolean = false
    ) : AbstractButton(style, weight, imprints) {
      /**
       * [Reaction] list that [Reaction.act]s when a user short-clicks the button.
       */
      val shortClicks = shortClicks.toList()

      protected fun toBuilderProtected(builder: Builder): Builder {
        super.toBuilderProtected(builder)
        builder.hasTimeLimit = hasTimeLimit
        builder.isOneWay = isOneWay
        shortClicks.mapTo(builder.shortClicks) { it.toBuilder() }
        return builder
      }

      abstract class Builder : AbstractButton.Builder() {
        var shortClicks = mutableListOf<Reaction.Builder>()
        var hasTimeLimit = false
        var isOneWay = false

        final override fun shortClickReaction(init: Reaction.Builder.() -> Unit) {
          shortClicks.add(Reaction.Builder().also(init))
        }
      }

      /**
       * [AbstractSwitcherButton] which a user can short-click several times to switch
       * composing action, and long-click to execute user-defined action.
       */
      class CustomLongClickButton(
        style: Style,
        weight: Float,
        imprints: List<ButtonText>,
        override val longClick: Reaction?,
        shortClicks: List<Reaction>,
        hasTimeLimit: Boolean = false,
        isOneWay: Boolean = false
      ) : AbstractSwitcherButton(
        style, weight, imprints, shortClicks, hasTimeLimit, isOneWay
      ), LongClickable, PopupRow.Popable {
        override fun toBuilder(): Builder = toBuilderProtected(
          Builder()
        )

        private fun toBuilderProtected(builder: Builder): Builder {
          super.toBuilderProtected(builder)
          builder.longClick = longClick?.toBuilder()
          return builder
        }

        class Builder : AbstractSwitcherButton.Builder(),
          LongClickableBuilder {
          override var longClick: Reaction.Builder? = null

          override fun build() = CustomLongClickButton(
            style, weight, imprints, longClick?.build(), shortClicks.map { it.build() }, hasTimeLimit, isOneWay
          )
        }
      }

      class PopupButton(
        style: Style,
        weight: Float,
        imprints: List<ButtonText>,
        val firstSection: PopupSection,
        val secondSection: PopupSection,
        val thirdSection: PopupSection,
        val fourthSection: PopupSection,
        shortClicks: List<Reaction>,
        hasTimeLimit: Boolean = false,
        isOneWay: Boolean = false
      ) : AbstractSwitcherButton(style, weight, imprints, shortClicks, hasTimeLimit, isOneWay) {
        override fun toBuilder(): Builder = toBuilderProtected(
          Builder()
        )

        private fun toBuilderProtected(builder: Builder): Builder {
          super.toBuilderProtected(builder)
          builder.firstSection = firstSection
          builder.secondSection = secondSection
          builder.thirdSection = thirdSection
          builder.fourthSection = fourthSection
          return builder
        }

        class Builder : AbstractSwitcherButton.Builder() {
          var firstSection = PopupSection(listOf())
          var secondSection = PopupSection(listOf())
          var thirdSection = PopupSection(listOf())
          var fourthSection = PopupSection(listOf())

          override fun build() = PopupButton(
            style, weight, imprints,
            firstSection, secondSection, thirdSection, fourthSection,
            shortClicks.map { it.build() }, hasTimeLimit, isOneWay
          )
        }
      }
    }
  }

  class PopupRow<T>(
    weight: Float,
    buttons: List<T>
  ) : KeyboardRow(weight, buttons) where T : AbstractButton, T : PopupRow.Popable {
    interface Popable
  }

  data class PopupSection(val rows: List<PopupRow<*>>)

  /**
   * Representation of row of row-based keyboard.
   */
  open class KeyboardRow(
    weight: Float,
    buttons: List<AbstractButton>
  ) : Weighable {
    final override val weight by NonNegativeFloat(weight)
    /**
     * [AbstractButton] list of this [KeyboardRow].
     */
    val buttons = buttons.toList()

    /**
     * Summation of all the [buttons] in this [KeyboardRow].
     */
    fun sumButtonWeight() = buttons.map { it.weight }.sum()

    fun toBuilder() = Builder().also { builder ->
      builder.weight = weight
      buttons.mapTo(builder.buttons) { it.toBuilder() }
    }

    class Builder {
      var weight = 1F
      var buttons = mutableListOf<AbstractButton.Builder>()

      fun build() = KeyboardRow(weight, buttons.map { it.build() })

      fun customLongClickButton(
        init: AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder.() -> Unit
      ) =
        buttons.add(AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder().also(init))

      fun baseButton(init: AbstractButton.AbstractNonSwitcherButton.BaseButton.Builder.() -> Unit) =
        buttons.add(AbstractButton.AbstractNonSwitcherButton.BaseButton.Builder().also(init))

      fun repeaterButton(init: AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Builder.() -> Unit) =
        buttons.add(AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Builder().also(init))
    }

    /**
     * Model to store width to measure the borderlines of the buttons.
     */
    inner class Model(
      width: Float
    ) {
      /**
       * Width of a keyboard view.
       */
      val width by NonNegativeFloat(width)
      /**
       * Stored value of [sumButtonWeight].
       */
      val totalButtonWeight = sumButtonWeight()

      /**
       * Measured button borderlines position list.
       */
      val buttonBorderlines =
        buttons.cumulate(0F) { acc, button -> acc + button.weight * width / totalButtonWeight }.toList()
    }
  }

  /**
   * Orientation of keyboard rows.
   */
  enum class Orientation {
    HORIZONTAL, VERTICAL
  }

  /**
   * Summation of all the [rows] in this [RowBasedKeyboard].
   */
  fun sumRowWeight() = rows.map { it.weight }.sum()

  data class ButtonRect(val left: Float, val top: Float, val right: Float, val bottom: Float, val button: AbstractButton)

  /**
   * A pair of [KeyboardRow.Model] and [borderline].
   */
  data class RowModelBorderline(val model: KeyboardRow.Model, val borderline: Float)

  /**
   * An index pair of [KeyboardRow] in [RowBasedKeyboard] and [AbstractButton] in [KeyboardRow].
   */
  class RowAndButtonIndex private constructor(
    /**
     * Index of [KeyboardRow] in [RowBasedKeyboard].
     * Valid value that the instance can exist is greater than -1.
     * -1 means 'undefined', and forces [buttonIndex] to be -1 as well.
     *
     * @see buttonIndex
     */
    val rowIndex: Int = -1,
    /**
     * Index of [AbstractButton] in [KeyboardRow].
     * Valid value that the instance can exist is greater than -1.
     * -1 means 'undefined'. It should be -1 as well if [rowIndex] is -1.
     *
     * @see rowIndex
     */
    val buttonIndex: Int = -1
  ) {
    init {
      require(rowIndex >= -1)
      require(buttonIndex >= -1)
      // There should not be a selected button in an undefined row
      require(rowIndex != -1 || buttonIndex == -1)
    }

    companion object {
      /**
       * Only one [RowAndButtonIndex] whose [RowAndButtonIndex.rowIndex] is -1 and
       * [RowAndButtonIndex.buttonIndex] is -1 simultaneously.
       */
      val rowIndexOutOfBounds = RowAndButtonIndex()

      /**
       * It determines necessity to create new [RowAndButtonIndex] instance, and returns
       * proper [RowAndButtonIndex].
       *
       * @return [rowIndexOutOfBounds] if [rowIndex] and [buttonIndex] are all -1,
       * newly created [RowAndButtonIndex] instance otherwise.
       */
      fun create(
        /**
         * [RowAndButtonIndex.rowIndex] of being returned [RowAndButtonIndex].
         */
        rowIndex: Int = -1,
        /**
         * [RowAndButtonIndex.buttonIndex] of being returned [RowAndButtonIndex].
         */
        buttonIndex: Int = -1,
        /**
         * It prevents throwing exception when [rowIndex] is -1 but [buttonIndex] is not.
         * Returned [RowAndButtonIndex.buttonIndex] is fixed by -1.
         *
         * @see RowAndButtonIndex.rowIndex
         */
        correctionByHierarchy: Boolean = false,
        /**
         * It prevents throwing exception when [rowIndex] or [buttonIndex] is less than -1.
         * Returned [RowAndButtonIndex]'s [RowAndButtonIndex.rowIndex] or
         * [RowAndButtonIndex.buttonIndex] is -1 if each corresponding [rowIndex] or
         * [buttonIndex] is less than -1.
         */
        correctionByRange: Boolean = false
      ): RowAndButtonIndex {
        val correctRowIndex =
          if (
            correctionByRange && rowIndex < 0
          ) { -1 }
          else { rowIndex }
        val correctButtonIndex =
          if (
            correctionByRange && buttonIndex < 0 || correctionByHierarchy && correctRowIndex < 0
          ) { -1 }
          else { buttonIndex }

        return if (correctRowIndex == -1 && correctButtonIndex == -1) rowIndexOutOfBounds
        else RowAndButtonIndex(correctRowIndex, correctButtonIndex)
      }
    }
  }

  inner class PopupModel private constructor(
    width: Float,
    height: Float,
    popupButton: AbstractButton.AbstractSwitcherButton.PopupButton
  ) {
    val width by NonNegativeFloat(width)
    val height by NonNegativeFloat(height)
    val totalRowWeight = sumRowWeight()

    private val rowBorderlines =
      popupButton.firstSection.rows.cumulate(0F) { acc, row -> acc + row.weight * height / totalRowWeight }.toList()
    private val rowModels =
      popupButton.firstSection.rows.map { it.Model(width) }
    val rowModelBorderlines =
      rowModels.zip(rowBorderlines) { rowModel, borderline -> RowModelBorderline(rowModel, borderline) }
  }

  /**
   * Model to store width and height to measure the borderlines of the buttons.
   */
  inner class Model(
    width: Float,
    height: Float
  ) : MetakeyboardLayout.ModelDelegate {
    /**
     * Width of keyboard view.
     */
    val width by NonNegativeFloat(width)
    /**
     * Height of keyboard view.
     */
    val height by NonNegativeFloat(height)
    /**
     * Stored value of [sumRowWeight].
     */
    val totalRowWeight = sumRowWeight()

    /**
     * [Rate] of the last clicked keyboard button.
     */
    var rate: AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Rate? = null
      private set
      get() = (lastDownButton as? AbstractButton.AbstractNonSwitcherButton.RepeaterButton)?.rate

    /**
     * Measured row borderlines position list.
     */
    private val rowBorderlines =
      rows.cumulate(0F) { acc, row -> acc + row.weight * height / totalRowWeight }.toList()
    /**
     * [KeyboardRow.Model] list with this [width].
     */
    private val rowModels =
      rows.map { it.Model(width) }
    /**
     * List of pair of [KeyboardRow.Model] and corresponding measured row borderline position.
     */
    val rowModelBorderlines =
      rowModels.zip(rowBorderlines) { rowModel, borderline -> RowModelBorderline(rowModel, borderline) }

    /**
     * Counter for iterating [Reaction]s in [AbstractButton.AbstractSwitcherButton].
     *
     * @see resetClickCounter
     */
    private var clickCounter = 0
    /**
     * Flag that means the button has received long click event.
     */
    private var isLongClicked = false
    // TODO: refactor
    private var isFastClicked = false
    /**
     * The last button that received touch down event.
     */
    private var lastDownButton: AbstractButton? = null
    /**
     * The last button that received touch up event.
     */
    private var lastUpButton: AbstractButton? = null

    var popupModel: PopupModel? = null

    /**
     * Reset the click counter for iterating [Reaction]s in
     * [AbstractButton.AbstractSwitcherButton].
     */
    fun resetClickCounter() {
      clickCounter = 0
    }

    override fun onTouch(
      x: Float, y: Float, touchAction: MetakeyboardLayout.ModelDelegate.TouchAction
    ) {
      fun notifyClickedButtonChanged() {
        isFastClicked = false
        MetakeyboardModel.integrationComposer.push()
      }

      val rowIndex =
        rowBorderlines.indexOfFirst { it > y }
      val buttonIndex =
        rowModelBorderlines.getOrNull(rowIndex)?.model?.buttonBorderlines?.indexOfFirst { it > x } ?: return
      // if user touches an empty keyboard or row, return@onTouch
      val currentButton =
        rows.getOrNull(rowIndex)?.buttons?.getOrNull(buttonIndex) ?: return

      when (touchAction) {
        MetakeyboardLayout.ModelDelegate.TouchAction.DOWN -> {
          lastDownButton = currentButton
        }
        MetakeyboardLayout.ModelDelegate.TouchAction.MOVE -> {
          when (currentButton) {
            is AbstractButton.AbstractSwitcherButton.CustomLongClickButton -> {
              popupModel
            }
          }
        }
        MetakeyboardLayout.ModelDelegate.TouchAction.UP -> {
          when (currentButton) {
            is AbstractButton.AbstractNonSwitcherButton.BaseButton -> {
              if (lastDownButton === currentButton) {
                currentButton.shortClick.act()
                notifyClickedButtonChanged()
              }
            }
            is AbstractButton.AbstractNonSwitcherButton.RepeaterButton -> {
              if (lastDownButton === currentButton && !isLongClicked) {
                currentButton.shortClick.act()
                notifyClickedButtonChanged()
              }
            }
            is AbstractButton.AbstractSwitcherButton.CustomLongClickButton -> {
              if (lastDownButton === currentButton) {
                if (!isLongClicked || currentButton.longClick == null) {
                  if (
                    lastUpButton !== currentButton
                    || currentButton.hasTimeLimit && !isFastClicked
                    || currentButton.shortClicks.size == 1
                  ) {
                    clickCounter = 0
                    notifyClickedButtonChanged()
                  }
                  if (currentButton.shortClicks.isNotEmpty()) {
                    MetakeyboardModel.integrationComposer.clearVolatile()
                    currentButton.shortClicks[clickCounter].act()
                    clickCounter = (clickCounter + 1) % currentButton.shortClicks.size
                  }
                }
              }
            }
            is AbstractButton.AbstractSwitcherButton.PopupButton -> {

              popupModel = null
              notifyClickedButtonChanged()
            }
          }
          isLongClicked = false
          if (lastDownButton === currentButton) {
            lastUpButton = currentButton
          }
          isFastClicked = true
        }
      }
    }

    override fun onTime(timerEvent: MetakeyboardLayout.ModelDelegate.TimerEvent) {
      lastDownButton ?: return

      val repeaterButtonAct = { repeaterButton: AbstractButton.AbstractNonSwitcherButton.RepeaterButton ->
        repeaterButton.longClick?.act() ?: repeaterButton.shortClick.act()
        MetakeyboardModel.integrationComposer.push()
      }

      // val for smart casting
      val fetchedLastDownButton = lastDownButton
      when (timerEvent) {
        MetakeyboardLayout.ModelDelegate.TimerEvent.LONG_CLICK -> {
          isLongClicked = true
          MetakeyboardModel.integrationComposer.push()
          when (fetchedLastDownButton) {
            is AbstractButton.AbstractNonSwitcherButton.BaseButton -> {
            }
            is AbstractButton.AbstractNonSwitcherButton.RepeaterButton -> {
              repeaterButtonAct(fetchedLastDownButton)
            }
            is AbstractButton.AbstractSwitcherButton.CustomLongClickButton -> {
              fetchedLastDownButton.longClick?.act()
            }
            is AbstractButton.AbstractSwitcherButton.PopupButton -> {
              popupModel
            }
          }
        }
        MetakeyboardLayout.ModelDelegate.TimerEvent.SLOW_CLICK -> {
          isFastClicked = false
        }
        MetakeyboardLayout.ModelDelegate.TimerEvent.REPEAT -> {
          when (fetchedLastDownButton) {
            is AbstractButton.AbstractNonSwitcherButton.RepeaterButton -> repeaterButtonAct(fetchedLastDownButton)
            else -> {
            }
          }
        }
      }
    }

    /**
     * It locates [RowAndButtonIndex] position on the keyboard with [x]-[y] coordinate.
     *
     * @return [RowAndButtonIndex] of the button on the [x]-[y] coordinate.
     */
    fun getRowAndButtonIndex(x: Float, y: Float): RowAndButtonIndex {
      val rowIndex =
        rowBorderlines.indexOfFirst { it > y }
      val buttonIndex =
        rowModelBorderlines.getOrNull(rowIndex)?.model?.buttonBorderlines?.indexOfFirst { it > x } ?: -1
      return RowAndButtonIndex.create(rowIndex, buttonIndex)
    }
  }
}