package org.metakeyboard.core

import org.metakeyboard.languagecomposerimpl.Hangul
import kotlin.reflect.KClass

/**
 * This exists because Kotlin or Java Reflection can't search a class in a package.
 * LanguageComposer authors should add their LanguageComposer class in here.
 * Please add one in alphabetical order.
 */
fun findLanguageComposer(languageComposer: String): KClass<out LanguageComposer> =
  when (languageComposer) {
    "Hangul" -> Hangul::class
    else -> throw IllegalArgumentException()
  }