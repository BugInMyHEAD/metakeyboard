package org.metakeyboard.core

import kotlin.reflect.KClass

/**
 * Common interface inherited by [IntegrationComposer] and [LanguageComposer].
 *
 * @see[IntegrationComposer]
 * @see[LanguageComposer]
 */
interface PrimitiveComposer {
  /**
   * It stores given [composee].
   *
   * @param[composee] [PrimitiveComposer.Composee] to be composed.
   */
  fun push(composee: Composee)

  /**
   * It returns committed [CharSequence] after composing.
   */
  fun pullCommitted(): CharSequence
  /**
   * It returns uncommitted [CharSequence] after composing.
   */
  fun pullUncommitted(): CharSequence

  /**
   * It clears committed [CharSequence].
   */
  fun clearCommitted()
  /**
   * It clears uncommitted [CharSequence] and all pushed [Composee]s.
   */
  fun clearUncommitted()

  /**
   * The basic characteristic of [Composee] that affects [PrimitiveComposer]'s
   * composing behavior.
   */
  enum class Category {
    /**
     * Named elements need to be 'named' because they are not graphemes whoever
     * knows if (s)he learn the certain language.
     *
     * They are usually necessary to diverse combinations of [Composee]s
     * to reduce the number of buttons of a keyboard
     * in mobile devices which have a small screen space to spare to the keyboard.
     */
    NAMED_ELEMENT,
    /**
     * https://en.wikipedia.org/wiki/Grapheme
     *
     * Graphemes are able to be in use without arbitrary name,
     * usually able to be input with standard desktop hardware keyboards.
     */
    GRAPHEME,
    /**
     * It is for users to print [Composee.value] as they want.
     */
    TEXT
  }

  /**
   * The unit composed by [PrimitiveComposer] by being pushed to [PrimitiveComposer].
   */
  data class Composee(
    /**
     * The [Category] of this [Composee] that describe meaning of [value].
     */
    val category: Category,
    /**
     * With meaning presented by [category], it determines state transfer of each
     * [IntegrationComposer] or [LanguageComposer].
     */
    val value: String,
    /**
     * [LanguageComposer] to compose this [Composee], if it is not null. Otherwise, the
     * [IntegrationComposer] handle this [Composee].
     */
    val languageComposer: KClass<out LanguageComposer>?,
    /**
     * Text can be displayed in a text input window of a user application running on OS.
     * The actual text displayed may differ by each [PrimitiveComposer]'s composing.
     */
    val print: String? = null
  )
}