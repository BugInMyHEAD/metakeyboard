package org.metakeyboard.core

/**
 * It provides a way to composing with basic [PrimitiveComposer.Composee] inputs.
 *
 * 'Volatile [PrimitiveComposer.Composee]' means one that can be cleared by [clearVolatile].
 * 'Stable [PrimitiveComposer.Composee]' means one that is not 'volatile'.
 */
interface IntegrationComposer : PrimitiveComposer {
  /**
   * It receives preceding [CharSequence] in such as EditText(of Android).
   * Necessary to implement behavior like 'backspace' key's.
   */
  var lackOfCommittedTextEventHandler: () -> CharSequence

  /**
   * It stores given [composee] if [composee] is not null. It makes all currently volatile
   * [PrimitiveComposer.Composee]s stable if [volatility] is true, and pushes [composee] as
   * a stable one.
   *
   * If you want for all currently volatile [PrimitiveComposer.Composee]s to be stable,
   * and doesn't want to push any [PrimitiveComposer.Composee] use this method like the following
   * code snippet:
   * ```
   * push()
   * ```
   * ; because that code doesn't push any [PrimitiveComposer.Composee] but let all of stored
   * [PrimitiveComposer.Composee]s be stable.
   *
   * @param[composee] [PrimitiveComposer.Composee] to be composed.
   * @param[volatility] The volatility of [composee].
   */
  fun push(composee: PrimitiveComposer.Composee? = null, volatility: Boolean = false)

  /**
   * Equal to the following code:
   * ```
   * push(composee, false)
   * ```
   */
  override fun push(composee: PrimitiveComposer.Composee) {
    push(composee, false)
  }

  /**
   * It clears all volatile [PrimitiveComposer.Composee].
   */
  fun clearVolatile()
}