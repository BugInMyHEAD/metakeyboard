package org.metakeyboard.core

// TODO: eliminate dependency to Java
import java.util.*

/**
 * Official Abstract Integration Composer for future development.
 * It provides basic attributes and essential method implementation for
 * convenient development of concrete [IntegrationComposer] implementation.
 */
abstract class MetakeyboardOfficialAbstractIntegrationComposer :
  MetakeyboardOfficialAbstractPrimitiveComposer(),
  IntegrationComposer {
  /**
   * It stores the [push]ed volatile [PrimitiveComposer.Composee]s.
   */
  protected val volatileComposees = LinkedList<PrimitiveComposer.Composee>()

  final override var lackOfCommittedTextEventHandler: () -> CharSequence = { "" }

  final override fun push(composee: PrimitiveComposer.Composee?, volatility: Boolean) {
    if (volatility) {
      composee?.let {
        makeDirty()
        volatileComposees.add(it)
      }
    } else {
      makeDirty()
      stableComposees.addAll(volatileComposees)
      volatileComposees.clear()
      composee?.let { stableComposees.add(it) }
    }
  }

  final override fun push(composee: PrimitiveComposer.Composee) {
    super.push(composee)
  }

  final override fun clearCommitted() {
    super.clearCommitted()
  }

  final override fun clearUncommitted() {
    clearVolatile()
    stableComposees.clear()
  }

  final override fun clearVolatile() {
    makeDirty()
    volatileComposees.clear()
  }
}