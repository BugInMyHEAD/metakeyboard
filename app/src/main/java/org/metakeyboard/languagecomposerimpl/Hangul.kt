package org.metakeyboard.languagecomposerimpl

import org.metakeyboard.common.dumpTo
import org.metakeyboard.core.MetakeyboardOfficialAbstractLanguageComposer
import org.metakeyboard.core.PrimitiveComposer

class Hangul : MetakeyboardOfficialAbstractLanguageComposer() {
  companion object {
    private const val BASE_CODE = '가'.toInt() // '\uAC00'
    //private const val INITIAL_CONSONANT_COUNT = 19
    private const val VOWEL_COUNT = 21
    /**
     * This count includes case when there is no final consonant.
     */
    private const val FINAL_CONSONANT_COUNT = 28

    private const val NO_FINAL_CONSONANT = '\u0000'

    private val NAMED_ELEMENT_DEFAULT = mapOf(
      // Please enumerate in alphabetical order
      "ko_kr_h_add_stroke" to NamedElementDefault("", Type.AllHelper),
      "ko_kr_h_ceon" to NamedElementDefault(
        "·",
        Type.VowelOnlyHelper
      ),
      "ko_kr_h_ceonceon" to NamedElementDefault(
        "‥",
        Type.VowelOnlyHelper
      ),
      "ko_kr_h_double_consonant" to NamedElementDefault(
        "",
        Type.ConsonantOnlyHelper
      )
    )

    /**
     * Utility extension property to get first [Char] of [PrimitiveComposer.Composee.value]
     * only when [PrimitiveComposer.Composee.category] is [PrimitiveComposer.Category.GRAPHEME].
     * Otherwise, it throws [NoSuchElementException].
     */
    private val PrimitiveComposer.Composee.grapheme
      get() =
        if (category == PrimitiveComposer.Category.GRAPHEME && value.length == 1) value[0]
        else throw NoSuchElementException()

    /**
     * Utility extension property to get [PrimitiveComposer.Composee.value]
     * only when [PrimitiveComposer.Composee.category] is [PrimitiveComposer.Category.NAMED_ELEMENT].
     * Otherwise, it throws [NoSuchElementException].
     */
    private val PrimitiveComposer.Composee.namedElement
      get() =
        if (category == PrimitiveComposer.Category.NAMED_ELEMENT) value
        else throw NoSuchElementException()

    /**
     * Utility extension property to get proper print text.
     *
     * @return Empty string if [this] is null,
     * default value in this class if [PrimitiveComposer.Composee.print] is null,
     * [PrimitiveComposer.Composee.print] if that is not null.
     *
     * @see NAMED_ELEMENT_DEFAULT
     */
    private val PrimitiveComposer.Composee?.actualPrint
      get() = when {
        this == null ->
          ""
        category == PrimitiveComposer.Category.NAMED_ELEMENT ->
          print ?: NAMED_ELEMENT_DEFAULT.getValue(value).print
        else ->
          value
      }

    private val PrimitiveComposer.Composee.canBeChoseong
      get() = runCatching { getChoseongIndex(grapheme) }.isSuccess

    private val PrimitiveComposer.Composee.canBeJungseong
      get() = type is Type.Vowel

    private val PrimitiveComposer.Composee.canBeJongseong
      get() = runCatching { getJongseongIndex(grapheme) }.isSuccess

    private infix fun Char.combineConsonant(soleConsonant: Char) =
      jaDictionary(this, soleConsonant).let { if (it == '\u0000') null else it }

    private infix fun PrimitiveComposer.Composee?.combineConsonant(
      consonantType: PrimitiveComposer.Composee?
    ): PrimitiveComposer.Composee? {
      return when {
        this == null -> consonantType

        consonantType == null -> this

        else -> PrimitiveComposer.Composee(
          PrimitiveComposer.Category.GRAPHEME,

          when (consonantType.type) {
            is Type.SingleConsonant -> {
              (grapheme combineConsonant consonantType.grapheme) ?: return null
            }
            is Type.ConsonantHelper -> when (consonantType.value) {
              "ko_kr_h_add_stroke" -> getStrokeAddedConsonantOrVowel(grapheme)
              "ko_kr_h_double_consonant" -> getDoubleConsonant(grapheme)
              else -> return null
            }
            else -> {
              return null
            }
          }.toString(),

          Hangul::class
        )
      }
    }

    private infix fun PrimitiveComposer.Composee?.combineVowel(
      vowelType: PrimitiveComposer.Composee?
    ): PrimitiveComposer.Composee? {
      return when {
        this == null -> vowelType

        vowelType == null -> this

        else -> PrimitiveComposer.Composee(
          PrimitiveComposer.Category.GRAPHEME,

          if (vowelType.value == "ko_kr_h_add_stroke") {
            try {
              getStrokeAddedConsonantOrVowel(grapheme).toString()
            } catch (exc: NoSuchElementException) {
              return null
            }
          } else when (value) {
            "ko_kr_h_ceon" -> when (vowelType.value) {
              "ㅣ" -> "ㅓ"; "ㅡ" -> "ㅗ"; else -> return null
            }
            "ko_kr_h_ceonceon" -> when (vowelType.value) {
              "ㅣ" -> "ㅕ"; "ㅡ" -> "ㅛ"; else -> return null
            }
            "ㅏ" -> when (vowelType.value) {
              "ㅣ" -> "ㅐ"; "ko_kr_h_ceon" -> "ㅑ"; else -> return null
            }
            "ㅑ" -> when (vowelType.value) {
              "ㅣ" -> "ㅒ"; "ko_kr_h_ceon" -> "ㅏ"; else -> return null
            }
            "ㅓ" -> when (vowelType.value) {
              "ㅣ" -> "ㅔ"; else -> return null
            }
            "ㅕ" -> when (vowelType.value) {
              "ㅣ" -> "ㅖ"; else -> return null
            }
            "ㅗ" -> when (vowelType.value) {
              "ㅣ" -> "ㅚ"; "ㅏ" -> "ㅘ"; "ㅐ" -> "ㅙ"; else -> return null
            }
            "ㅚ" -> when (vowelType.value) {
              "ko_kr_h_ceon" -> "ㅘ"; else -> return null
            }
            "ㅜ" -> when (vowelType.value) {
              "ㅣ" -> "ㅟ"; "ㅓ" -> "ㅝ"; "ㅔ" -> "ㅞ"; "ko_kr_h_ceon" -> "ㅠ"; else -> return null
            }
            "ㅠ" -> when (vowelType.value) {
              "ㅣ" -> "ㅝ"; "ko_kr_h_ceon" -> "ㅜ"; else -> return null
            }
            "ㅝ" -> when (vowelType.value) {
              "ㅣ" -> "ㅞ"; else -> return null
            }
            "ㅘ" -> when (vowelType.value) {
              "ㅣ" -> "ㅙ"; else -> return null
            }
            "ㅡ" -> when (vowelType.value) {
              "ㅣ" -> "ㅢ"; "ko_kr_h_ceon" -> "ㅜ"; "ko_kr_h_ceonceon" -> "ㅠ"; else -> return null
            }
            "ㅣ" -> when (vowelType.value) {
              "ko_kr_h_ceon" -> "ㅏ"; "ko_kr_h_ceonceon" -> "ㅑ"; else -> return null
            }
            else -> return null
          },

          Hangul::class
        )
      }
    }

    /**
     * Sequentially combines consonants.
     *
     * @see combineConsonant
     */
    private fun combineConsonants(
      consonants: Iterable<PrimitiveComposer.Composee>
    ): PrimitiveComposer.Composee? =
      consonants
        .fold<PrimitiveComposer.Composee, PrimitiveComposer.Composee?>(null) { acc, composee ->
          acc combineConsonant composee
        }


    /**
     * Sequentially combines vowels.
     *
     * @see combineVowel
     */
    private fun combineVowels(
      vowels: Iterable<PrimitiveComposer.Composee>
    ): PrimitiveComposer.Composee? =
      vowels
        .fold<PrimitiveComposer.Composee, PrimitiveComposer.Composee?>(null) { acc, composee ->
          acc combineVowel composee
        }

    private fun getChoseongIndex(choseong: Char) = when (choseong) {
      'ㄱ' -> 0; 'ㄲ' -> 1
      'ㄴ' -> 2
      'ㄷ' -> 3; 'ㄸ' -> 4
      'ㄹ' -> 5
      'ㅁ' -> 6
      'ㅂ' -> 7; 'ㅃ' -> 8
      'ㅅ' -> 9;  'ㅆ' -> 10
      'ㅇ' -> 11
      'ㅈ' -> 12; 'ㅉ' -> 13
      'ㅊ' -> 14; 'ㅋ' -> 15; 'ㅌ' -> 16; 'ㅍ' -> 17; 'ㅎ' -> 18
      else -> throw IllegalArgumentException()
    }

    private fun getJungseongIndex(jungseong: Char) = when (jungseong) {
      in 'ㅏ' .. 'ㅣ' -> jungseong - 'ㅏ'
      else -> throw IllegalArgumentException()
    }

    private fun getJongseongIndex(jongseong: Char) = when (jongseong) {
      '\u0000' -> 0
      'ㄱ' -> 1; 'ㄲ' -> 2; 'ㄳ' -> 3
      'ㄴ' -> 4; 'ㄵ' -> 5; 'ㄶ' -> 6
      'ㄷ' -> 7
      'ㄹ' -> 8; 'ㄺ' -> 9; 'ㄻ' -> 10; 'ㄼ' -> 11; 'ㄽ' -> 12; 'ㄾ' -> 13; 'ㄿ' -> 14; 'ㅀ' -> 15
      'ㅁ' -> 16
      'ㅂ' -> 17; 'ㅄ' -> 18
      'ㅅ' -> 19; 'ㅆ' -> 20
      'ㅇ' -> 21; 'ㅈ' -> 22; 'ㅊ' -> 23; 'ㅋ' -> 24; 'ㅌ' -> 25; 'ㅍ' -> 26; 'ㅎ' -> 27
      else -> throw IllegalArgumentException()
    }

    private fun getStrokeAddedConsonantOrVowel(choseong: Char) = when (choseong) {
      'ㄱ' -> 'ㅋ'; 'ㅋ' -> 'ㄱ'
      'ㄴ' -> 'ㄷ'; 'ㄷ' -> 'ㅌ'; 'ㅌ' -> 'ㄴ'
      'ㅁ' -> 'ㅂ'; 'ㅂ' -> 'ㅍ'; 'ㅍ' -> 'ㅁ'
      'ㅅ' -> 'ㅈ'; 'ㅈ' -> 'ㅊ'; 'ㅊ' -> 'ㅅ'
      'ㅇ' -> 'ㅎ'; 'ㅎ' -> 'ㅇ'
      'ㅏ' -> 'ㅑ'; 'ㅑ' -> 'ㅏ'
      'ㅓ' -> 'ㅕ'; 'ㅕ' -> 'ㅓ'
      'ㅗ' -> 'ㅛ'; 'ㅛ' -> 'ㅗ'
      'ㅜ' -> 'ㅠ'; 'ㅠ' -> 'ㅜ'
      else -> '\u0000'
    }

    private fun getDoubleConsonant(choseong: Char) = when (choseong) {
      'ㄱ' -> 'ㄲ'; 'ㄲ' -> 'ㄱ'
      'ㄷ' -> 'ㄸ'; 'ㄸ' -> 'ㄷ'
      'ㅂ' -> 'ㅃ'; 'ㅃ' -> 'ㅂ'
      'ㅈ' -> 'ㅉ'; 'ㅉ' -> 'ㅈ'
      'ㅅ' -> 'ㅆ'; 'ㅆ' -> 'ㅅ'
      else -> '\u0000'
    }

    private fun jaDictionary(lastJa: Char, currentJa: Char) = when (lastJa) {
      'ㄱ' -> when (currentJa) {
        'ㅅ' -> 'ㄳ'; else -> '\u0000'
      }
      'ㄴ' -> when (currentJa) {
        'ㅈ' -> 'ㄵ'; 'ㅎ' -> 'ㄶ'; else -> '\u0000'
      }
      'ㄹ' -> when (currentJa) {
        'ㄱ' -> 'ㄺ'; 'ㅁ' -> 'ㄻ'; 'ㅂ' -> 'ㄼ'; 'ㅅ' -> 'ㄽ'
        'ㅌ' -> 'ㄾ'; 'ㅍ' -> 'ㄿ'; 'ㅎ' -> 'ㅀ'; else -> '\u0000'
      }
      'ㅂ' -> when (currentJa) {
        'ㅅ' -> 'ㅄ'; else -> '\u0000'
      }
      else -> '\u0000'
    }

    /**
     * Utility extension property to get [Type].
     */
    private val PrimitiveComposer.Composee.type
      get() = when (category) {
        PrimitiveComposer.Category.GRAPHEME -> {
          when (grapheme) {
            in 'ㅏ' .. 'ㅣ' -> Type.VowelGrapheme

            'ㄱ', 'ㄴ', 'ㄷ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅅ',
            'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ' -> Type.SingleConsonant

            'ㄲ', 'ㄸ', 'ㅃ', 'ㅆ', 'ㅉ' -> Type.HomogeneousPairConsonant

            'ㄳ', 'ㄵ', 'ㄶ',
            'ㄺ', 'ㄻ', 'ㄼ', 'ㄽ', 'ㄾ', 'ㄿ', 'ㅀ',
            'ㅄ' -> Type.HeterogeneousPairConsonant

            else -> throw UnsupportedOperationException()
          }
        }
        PrimitiveComposer.Category.NAMED_ELEMENT -> {
          NAMED_ELEMENT_DEFAULT.getValue(namedElement).type
        }
        else -> {
          throw NoSuchElementException()
        }
      }

    private fun combineChoJungJong(
      choseong: Char, jungseong: Char, jongseong: Char = NO_FINAL_CONSONANT
    ) =
      (
          BASE_CODE +
              getChoseongIndex(choseong) * VOWEL_COUNT * FINAL_CONSONANT_COUNT +
              getJungseongIndex(jungseong) * FINAL_CONSONANT_COUNT +
              getJongseongIndex(jongseong)
      ).toChar()

    private fun completeChoJungJong(
      choseong: PrimitiveComposer.Composee?,
      jungseong: PrimitiveComposer.Composee?,
      jongseong: PrimitiveComposer.Composee?
    ) =
      if (jungseong == null) {
        if (choseong != null && jongseong != null) {
          throw UnsupportedOperationException()
        }
        (choseong?.actualPrint ?: "") + (jongseong?.actualPrint ?: "")
      } else when (jungseong.type) {
        is Type.VowelGrapheme -> {
          when {
            choseong == null ->
              jungseong.actualPrint + jongseong.actualPrint
            jongseong == null ->
              combineChoJungJong(choseong.grapheme, jungseong.grapheme).toString()
            else ->
              combineChoJungJong(choseong.grapheme, jungseong.grapheme, jongseong.grapheme).toString()
          }
        }
        is Type.VowelOnlyHelper -> {
          if (jongseong != null) {
            throw UnsupportedOperationException()
          }
          (choseong?.value ?: "") + jungseong.actualPrint
        }
        else -> throw NoWhenBranchMatchedException()
      }
  }

  private val currentChoseongMutableList = mutableListOf<PrimitiveComposer.Composee>()
  private val currentJungseongMutableList = mutableListOf<PrimitiveComposer.Composee>()
  /**
   * It means the first single consonant in a final consonant that is a pair consonant.
   */
  private val currentFixedJongseongMutableList = mutableListOf<PrimitiveComposer.Composee>()
  /**
   * It means the second single consonant in a final consonant that is a pair consonant.
   * It can move to [currentChoseongMutableList] on some situations.
   */
  private val currentFloatingJongseongMutableList = mutableListOf<PrimitiveComposer.Composee>()
  /**
   * It means pending named elements which haven't met proper vowel.
   * It can move to [currentJungseongMutableList] on those situations.
   */
  private val pendingJungseongMutableList = mutableListOf<PrimitiveComposer.Composee>()

  /**
   * Preview the result of folding combination of [currentChoseongMutableList].
   *
   * @see combineConsonants
   */
  private fun combineChoseongMutableList() =
    combineConsonants(currentChoseongMutableList)

  /**
   * Preview the result of folding combination of [currentJungseongMutableList].
   *
   * @see combineVowels
   */
  private fun combineJungseongMutableList() =
    combineVowels(currentJungseongMutableList)

  /**
   * Preview the result of folding combination of [currentFixedJongseongMutableList].
   *
   * @see combineConsonants
   */
  private fun combineFixedJongseongMutableList() =
    combineConsonants(currentFixedJongseongMutableList)

  /**
   * Preview the result of folding combination of [currentFloatingJongseongMutableList].
   *
   * @see combineConsonants
   */
  private fun combineFloatingJongseongMutableList() =
    combineConsonants(currentFloatingJongseongMutableList)

  /**
   * Preview the result of folding combination of [combinePendingJungseongMutableList].
   *
   * @see combineVowels
   */
  private fun combinePendingJungseongMutableList() =
    combineVowels(pendingJungseongMutableList)

  /**
   * Preview the result of folding combination of [currentFixedJongseongMutableList]
   * followed by [currentFloatingJongseongMutableList].
   *
   * @see combineConsonants
   */
  private fun combineJongseongMutableLists() =
    combineFixedJongseongMutableList() combineConsonant combineFloatingJongseongMutableList()

  /**
   * If there is a rational combination of Jongseong [PrimitiveComposer.Composee]s,
   * which is not null, all [PrimitiveComposer.Composee]s in [currentFloatingJongseongMutableList]
   * move to [currentFixedJongseongMutableList]. That means,
   * all Jongseong [PrimitiveComposer.Composee]s are able to be in a complete,
   * fixed Hangul Character.
   *
   * @see combineJongseongMutableLists
   */
  private fun dumpFloatingJongseong() {
    combineJongseongMutableLists() ?: return
    currentFloatingJongseongMutableList.dumpTo(currentFixedJongseongMutableList)
  }

  /**
   * It appends complete, fixed Hangul character to [committedBuilder], increases
   * [consumedComposeeCounter], and clear states to prepare next combination of Hangul
   * character.
   *
   * @see completeChoJungJong
   */
  private fun disposeCompleteHangulChar() {
    committedBuilder.append(
      completeChoJungJong(
        combineChoseongMutableList(),
        combineJungseongMutableList(),
        combineFixedJongseongMutableList()
      )
    )
    consumedComposeeCounter +=
      currentChoseongMutableList.size +
          currentJungseongMutableList.size +
          currentFixedJongseongMutableList.size
    currentChoseongMutableList.clear()
    currentJungseongMutableList.clear()
    currentFixedJongseongMutableList.clear()
  }

  private fun updateChoseong(consonant: PrimitiveComposer.Composee) {
    assert(consonant.type !is Type.Vowel)
    (combineChoseongMutableList() combineConsonant consonant).run {
      when {
        this != null && canBeChoseong -> {
          assert(type !is Type.HeterogeneousPairConsonant)
          currentChoseongMutableList.add(consonant)
        }
        this != null && canBeJongseong -> {
          assert(type is Type.HeterogeneousPairConsonant)
          currentChoseongMutableList.dumpTo(currentFixedJongseongMutableList)
          currentFloatingJongseongMutableList.add(consonant)
        }
        else -> {
          disposeCompleteHangulChar()
          currentChoseongMutableList.add(consonant)
        }
      }
    }
  }

  private fun updateJungseong(vowel: PrimitiveComposer.Composee) {
    assert(vowel.type !is Type.Consonant)
    (combineJungseongMutableList() combineVowel vowel).run {
      if (this == null || !canBeJungseong) {
        disposeCompleteHangulChar()
      }
    }
    currentJungseongMutableList.add(vowel)
  }

  private fun updateJongseong(consonant: PrimitiveComposer.Composee) {
    val newFloatingJongseongCandidate =
      combineFloatingJongseongMutableList() combineConsonant consonant
    val newJongseongCandidate =
      if (newFloatingJongseongCandidate == null) null
      else combineFixedJongseongMutableList() combineConsonant newFloatingJongseongCandidate

    val newFloatingJongseongCandidateIsHeterogeneous =
      newFloatingJongseongCandidate == null
          || newFloatingJongseongCandidate.type is Type.HeterogeneousPairConsonant

    when {
      currentFixedJongseongMutableList.isNotEmpty() -> {
        if (newJongseongCandidate == null && newFloatingJongseongCandidateIsHeterogeneous) {
          dumpFloatingJongseong()
          disposeCompleteHangulChar()
          updateChoseong(consonant)
        } else {
          currentFloatingJongseongMutableList.add(consonant)
        }
      }
      currentFloatingJongseongMutableList.isEmpty() || consonant.type is Type.ConsonantHelper -> {
        currentFloatingJongseongMutableList.add(consonant)
      }
      newFloatingJongseongCandidateIsHeterogeneous -> {
        dumpFloatingJongseong()
        currentFloatingJongseongMutableList.add(consonant)
      }
    }
  }

  /**
   * It can't distinguish heterogeneous pair consonants a user pushed.
   * If they exists in `stableComposees`, `IntegrationComposer` may do undefined
   * behavior because `consumedCount()` won't be matched.
   */
  private fun updateTearing(vowel: PrimitiveComposer.Composee) {
    if (vowel.type is Type.VowelOnlyHelper) {
      if (combinePendingJungseongMutableList() combineVowel vowel == null) {
        disposeCompleteHangulChar()
        committedBuilder.append(combinePendingJungseongMutableList().actualPrint)
        consumedComposeeCounter += pendingJungseongMutableList.size
        pendingJungseongMutableList.clear()
      }
      pendingJungseongMutableList.add(vowel)
    } else {
      disposeCompleteHangulChar()
      assert(combineFloatingJongseongMutableList()?.canBeChoseong ?: true)
      currentFloatingJongseongMutableList.dumpTo(currentChoseongMutableList)
      pendingJungseongMutableList.dumpTo(currentJungseongMutableList)
      currentJungseongMutableList.add(vowel)
    }
  }

  override fun compose() {
    clearCommitted()
    uncommittedBuilder.clear()

    currentChoseongMutableList.clear()
    currentJungseongMutableList.clear()
    currentFixedJongseongMutableList.clear()
    currentFloatingJongseongMutableList.clear()
    pendingJungseongMutableList.clear()

    stableComposees.forEach {
      assert(currentFixedJongseongMutableList.isEmpty() || currentFloatingJongseongMutableList.isNotEmpty())
      when (it.type) {
        is Type.Consonant -> {
          if (currentJungseongMutableList.isEmpty() && currentFloatingJongseongMutableList.isEmpty()) updateChoseong(it)
          else updateJongseong(it)
        }
        is Type.Vowel -> {
          if (currentFloatingJongseongMutableList.isEmpty()) updateJungseong(it)
          else updateTearing(it)
        }
        is Type.AllHelper -> when {
          currentFloatingJongseongMutableList.isNotEmpty() -> updateJongseong(it)
          currentJungseongMutableList.isNotEmpty() -> updateJungseong(it)
          currentChoseongMutableList.isNotEmpty() -> updateChoseong(it)
        }
      }
    }
    uncommittedBuilder
      .append(
        completeChoJungJong(
          combineChoseongMutableList(),
          combineJungseongMutableList(),
          (
              combineJongseongMutableLists()
                ?: combineFixedJongseongMutableList()
                ?: combineFloatingJongseongMutableList()
              )?.run { if (canBeJongseong) this else null }
        )
      ).append(
        if (combineJongseongMutableLists().run { this != null && canBeJongseong }) ""
        else combineFloatingJongseongMutableList().actualPrint
      ).append(
        combinePendingJungseongMutableList().actualPrint
      )
  }

  /**
   * It categorizes Hangul graphemes and named elements.
   */
  private sealed class Type {
    /**
     * It means Hangul single consonant graphemes including ㄱ, ㄴ, ...
     */
    object SingleConsonant : Consonant()
    /**
     * It means Hangul homogeneous pair consonant graphemes including ㄲ, ㄸ, ...
     */
    object HomogeneousPairConsonant : PairConsonant()
    /**
     * It means Hangul heterogeneous pair consonant graphemes including ㄳ, ㄻ, ...
     */
    object HeterogeneousPairConsonant : PairConsonant()
    /**
     * It means Hangul named elements treated as consonants in this class
     * including ko_kr_h_double_consonant, ...
     */
    object ConsonantOnlyHelper : Consonant(),
      ConsonantHelper
    /**
     * It means Hangul vowel graphemes including ㅏ, ㅓ, ...
     */
    object VowelGrapheme : Vowel()
    /**
     * It means Hangul named elements treated as vowels in this class
     * including ko_kr_h_ceon, ko_kr_h_ceonceon, ...
     */
    object VowelOnlyHelper : Vowel(), VowelHelper
    /**
     * It means Hangul named elements which can't be treated as just consonants or just vowels.
     * Including ko_kr_h_add_stroke, ...
     */
    object AllHelper : Type(), ConsonantHelper,
      VowelHelper

    /**
     * It means Hangul named elements.
     */
    interface Helper
    /**
     * It means Hangul named elements which can be treated as consonants in this class.
     */
    interface ConsonantHelper : Helper
    /**
     * It means Hangul named elements which can be treated as vowels in this class.
     */
    interface VowelHelper : Helper

    /**
     * It means Hangul composees treated as consonants.
     */
    abstract class Consonant : Type()
    /**
     * It means Hangul composees treated as pair consonants.
     */
    abstract class PairConsonant : Consonant()
    /**
     * It means Hangul composees treated as vowels.
     */
    abstract class Vowel : Type()
  }

  /**
   * It grants default values to Hangul named elements.
   *
   * @property[print] Default print text when the [PrimitiveComposer.Composee.print] is null.
   * @property[type] The deterministic [Type] of the Hangul named element.
   */
  private data class NamedElementDefault(val print: String, val type: Type)
}