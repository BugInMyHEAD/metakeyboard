package org.metakeyboard.common

/**
 * It moves all elements in this [MutableCollection] to another [MutableCollection].
 *
 * @receiver [MutableCollection] whose all elements are to be taken away to [destination].
 *
 * @param [destination] [MutableCollection] that [MutableCollection.addAll] elements which was in
 * the receiver.
 */
fun <E> MutableCollection<E>.dumpTo(destination: MutableCollection<in E>) {
  destination.addAll(this)
  clear()
}