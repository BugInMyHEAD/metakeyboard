package org.metakeyboard.common

/**
 * It creates sequence of cumulative(reduction) results.
 *
 * Thanks to Wasabi375
 * https://discuss.kotlinlang.org/u/Wasabi375
 * https://discuss.kotlinlang.org/t/reductions-cumulative-sum/8364/6
 */
fun <T, R> Iterable<T>.cumulate(initial: R, operation: (acc: R, T) -> R) = sequence {
  var last = initial
  forEach {
    last = operation(last, it)
    yield(last)
  }
}