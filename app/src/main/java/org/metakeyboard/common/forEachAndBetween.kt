package org.metakeyboard.common

/**
 * It executes [forElement] for each element in the receiver, and executes [betweenElements]
 * between two sequential elements for them.
 */
fun <T> Iterable<T>.forEachAndBetween(
  forElement: (el: T) -> Unit, betweenElements: (prev: T, next: T) -> Unit
) {
  iterator().run {
    if (!hasNext()) return
    var previousElement = next()

    forElement(previousElement)

    if (!hasNext()) return
    var nextElement = next()

    while (true) {
      betweenElements(previousElement, nextElement)
      forElement(nextElement)
      if (!hasNext()) {
        break
      }
      previousElement = nextElement
      nextElement = next()
    }
  }
}