package org.metakeyboard.common

import kotlin.reflect.KProperty

class NonNegativeFloat(
    private var realValue: Float
) : Comparable<Float> {
  override fun compareTo(other: Float): Int = when {
    realValue < other -> -1
    realValue > other ->  1
    else -> 0
  }

  operator fun getValue(thisRef: Any?, property: KProperty<*>) = realValue

  operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Float) {
    require(value >= 0) { "${property.name} value must be non-negative, was ${value}" }
    realValue = value
  }
}