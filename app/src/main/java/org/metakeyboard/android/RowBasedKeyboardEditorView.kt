package org.metakeyboard.android

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import org.metakeyboard.core.RowBasedKeyboard
import kotlin.properties.Delegates

class RowBasedKeyboardEditorView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : RowBasedKeyboardView(context, attrs, defStyleAttr, defStyleRes) {
  var selectedRowAndButtonIndex by Delegates.observable(
    RowBasedKeyboard.RowAndButtonIndex.rowIndexOutOfBounds
  ) { _, _, _ ->
    invalidate()
  }

  private val selectionRect = RectF()
  private val selectionRectPaint = Paint().apply {
    color = Color.BLACK
    strokeWidth = 20.0F
    style = Paint.Style.STROKE
  }

  override fun onDraw(canvas: Canvas) {
    super.onDraw(canvas)

    keyboardModel?.rowModelBorderlines?.run {
      // Top of the current row == Bottom of the previous row
      selectionRect.top =
        getOrNull(selectedRowAndButtonIndex.rowIndex - 1)?.borderline ?: 0F
      getOrNull(selectedRowAndButtonIndex.rowIndex)?.run {
        selectionRect.left =
          model.buttonBorderlines.getOrNull(selectedRowAndButtonIndex.buttonIndex - 1) ?: 0F
        selectionRect.right =
          model.buttonBorderlines.getOrNull(selectedRowAndButtonIndex.buttonIndex) ?: model.width
        selectionRect.bottom =
          borderline
        canvas.clipRect(selectionRect)
        canvas.drawRect(selectionRect, selectionRectPaint)
      }
    }
  }
}