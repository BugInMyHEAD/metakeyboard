package org.metakeyboard.android

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import org.metakeyboard.core.RowBasedKeyboard

class EditorViewModel : ViewModel() {
  companion object {
    fun modifyKeyboard(
      fragment: Fragment,
      modifier: RowBasedKeyboard.Builder.(rowIndex: Int, buttonIndex: Int) -> Unit
    ) {
      ViewModelProviders.of(fragment.activity!!)[EditorViewModel::class.java].run {
        keyboard.value =
          keyboard.value!!.toBuilder().apply {
            ViewModelProviders.of(fragment.activity!!)[EditorViewModel::class.java]
              .selectedRowAndButtonIndex.value?.let { rowAndButtonIndex ->
              modifier(rowAndButtonIndex.rowIndex, rowAndButtonIndex.buttonIndex)
            }
          }.build()
      }
    }

    private fun newRowAndButtonIndex(
      keyboard: RowBasedKeyboard, rowAndButtonIndex: RowBasedKeyboard.RowAndButtonIndex
    ) =
      runCatching { keyboard.rows[rowAndButtonIndex.rowIndex] }.fold(
        {
          runCatching { it.buttons[rowAndButtonIndex.buttonIndex] }.fold(
            { rowAndButtonIndex },
            { RowBasedKeyboard.RowAndButtonIndex.create(rowAndButtonIndex.rowIndex) }
          )
        },
        {
          RowBasedKeyboard.RowAndButtonIndex.rowIndexOutOfBounds
        }
      )

    private fun newRowAndButtonIndex(
      keyboard: RowBasedKeyboard, rowIndex: Int, buttonIndex: Int
    ) =
      newRowAndButtonIndex(
        keyboard,
        RowBasedKeyboard.RowAndButtonIndex.create(rowIndex, buttonIndex)
      )

    private fun newNearestRowAndButtonIndex(
      keyboard: RowBasedKeyboard, rowAndButtonIndex: RowBasedKeyboard.RowAndButtonIndex
    ): RowBasedKeyboard.RowAndButtonIndex {
      return rowAndButtonIndex.run {
        fun newNearestRowAndButtonIndexWithValidRowIndex(
          validRowIndex: Int, buttonIndex: Int
        ): RowBasedKeyboard.RowAndButtonIndex {
          val buttons = keyboard.rows[validRowIndex].buttons
          return if (0 <= buttonIndex && buttonIndex < buttons.size) {
            if (validRowIndex == rowIndex) this
            else RowBasedKeyboard.RowAndButtonIndex.create(validRowIndex, buttonIndex)
          }
          else {
            RowBasedKeyboard.RowAndButtonIndex.create(
              validRowIndex,
              when {
                buttons.isEmpty() -> -1
                buttonIndex < 0 -> 0
                else -> buttons.size - 1
              }
            )
          }
        }

        when {
          keyboard.rows.isEmpty() ->
            RowBasedKeyboard.RowAndButtonIndex.rowIndexOutOfBounds
          0 <= rowIndex && rowIndex < keyboard.rows.size ->
            newNearestRowAndButtonIndexWithValidRowIndex(rowIndex, buttonIndex)
          rowIndex < 0 ->
            newNearestRowAndButtonIndexWithValidRowIndex(0, buttonIndex)
          else ->
            newNearestRowAndButtonIndexWithValidRowIndex(keyboard.rows.size - 1, buttonIndex)
        }
      }
    }

    private fun newNearestRowAndButtonIndex(
      keyboard: RowBasedKeyboard, rowIndex: Int, buttonIndex: Int
    ) =
      newNearestRowAndButtonIndex(
        keyboard,
        RowBasedKeyboard.RowAndButtonIndex.create(rowIndex, buttonIndex)
      )
  }

  var fileName = ""

  val keyboard =
    MutableLiveData<RowBasedKeyboard>()
  val selectedRowAndButtonIndex =
    MutableLiveData<RowBasedKeyboard.RowAndButtonIndex>()
  init {
    keyboard.observeForever {
      selectedRowAndButtonIndex.value?.let {
        newRationalRowAndButtonIndex(it)
      } ?: newRationalRowAndButtonIndex(0, 0)
    }
  }

  val selectedRow
    get() =
      selectedRowAndButtonIndex.value?.run { keyboard.value?.run { rows.getOrNull(rowIndex) } }
  val selectedButton
    get() =
      selectedRowAndButtonIndex.value?.run { selectedRow?.run { buttons.getOrNull(buttonIndex) } }

  fun newRationalRowAndButtonIndex(rowAndButtonIndex: RowBasedKeyboard.RowAndButtonIndex) {
    selectedRowAndButtonIndex.value =
      newNearestRowAndButtonIndex(keyboard.value!!, rowAndButtonIndex)
  }

  fun newRationalRowAndButtonIndex(rowIndex: Int, buttonIndex: Int) =
    newRationalRowAndButtonIndex(
      RowBasedKeyboard.RowAndButtonIndex.create(
        rowIndex, buttonIndex, correctionByHierarchy = true, correctionByRange = true
      )
    )

  fun moveToRationalRowAndButtonIndex(rowIndexOffset: Int, buttonIndexOffset: Int) =
    selectedRowAndButtonIndex.value?.run {
      newRationalRowAndButtonIndex(rowIndex + rowIndexOffset, buttonIndex + buttonIndexOffset)
    }
}