package org.metakeyboard.android

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_edit_button.*
import org.metakeyboard.common.forEachAndBetween
import org.metakeyboard.common.kclass
import org.metakeyboard.core.MetakeyboardLayout
import org.metakeyboard.core.PrimitiveComposer
import org.metakeyboard.core.RowBasedKeyboard
import org.metakeyboard.core.findLanguageComposer
import kotlin.reflect.KClass

class EditButtonFragment : Fragment() {
  companion object {
    private val addLongClickData = EditKeyboardButtonActionViewData(
      EditKeyboardButtonActionViewConstants.AddLongClickData
    )

    private val addShortClickData = EditKeyboardButtonActionViewData(
      EditKeyboardButtonActionViewConstants.AddShortClickData
    )
  }

  private val selectedButton
    get() =
      ViewModelProviders.of(activity!!)[EditorViewModel::class.java]
        .selectedButton
        ?: throw IllegalStateException("selectedButton is null")

  private val selectedRowAndButtonIndex
    get() =
      ViewModelProviders.of(activity!!)[EditorViewModel::class.java]
        .selectedRowAndButtonIndex.value
        ?: throw IllegalStateException("selectedButton is null")

  private val keyboardButtonActionRecyclerViewData = object {
    private val viewDataMutableList = mutableListOf<EditKeyboardButtonActionViewData>()
    private lateinit var recentlySelectedButton: RowBasedKeyboard.AbstractButton

    val size: Int
      get() {
        update()

        val rsb = recentlySelectedButton
        return viewDataMutableList.size +
            if (rsb is RowBasedKeyboard.AbstractButton.AbstractSwitcherButton) rsb.shortClicks.size else 0
      }

    operator fun get(index: Int): EditKeyboardButtonActionViewData {
      require(index >= 0)

      update()

      val rsb = recentlySelectedButton
      return (
          runCatching { viewDataMutableList[index] }
            .getOrElse {
              when (rsb) {
                is RowBasedKeyboard.AbstractButton.AbstractSwitcherButton -> {
                  EditKeyboardButtonActionViewData(
                    EditKeyboardButtonActionViewConstants.MultipleShortClickData,
                    rsb.shortClicks[index - viewDataMutableList.size]
                  )
                }
                else -> throw it
              }
            }
          )
    }

    fun positionToShortClickIndex(position: Int): Int {
      return if (
        get(position).editKeyboardButtonActionViewConstants
        == EditKeyboardButtonActionViewConstants.MultipleShortClickData
      ) { position - viewDataMutableList.size }
      else { -1 }
    }

    private fun update() {
      viewDataMutableList.clear()
      recentlySelectedButton = selectedButton

      val rsb = recentlySelectedButton
      // for long click action
      if (rsb is RowBasedKeyboard.AbstractButton.LongClickable) {
        viewDataMutableList.add(
          if (rsb.longClick == null) addLongClickData
          else EditKeyboardButtonActionViewData(
            EditKeyboardButtonActionViewConstants.LongClickData,
            rsb.longClick
          )
        )
      }
      // for short click actions
      when (rsb) {
        is RowBasedKeyboard.AbstractButton.AbstractSwitcherButton -> {
          viewDataMutableList.add(addShortClickData)
        }
        is RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton -> {
          viewDataMutableList.add(
            EditKeyboardButtonActionViewData(
              EditKeyboardButtonActionViewConstants.OnlyOneShortClickData,
              rsb.shortClick
            )
          )
        }
      }
    }
  }

  private val keyboardButtonActionAdapter =
    object : RecyclerView.Adapter<EditKeyboardButtonActionViewHolder>() {
      override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
      ): EditKeyboardButtonActionViewHolder {
        return EditKeyboardButtonActionViewHolder(
          EditKeyboardButtonActionView(activity!!).apply {
            layoutParams = ViewGroup.LayoutParams(
              ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
            )
          }
        ).apply {
          editKeyboardButtonActionView.run {
            keyboardButtonActionDeleteButton.setOnClickListener {
              when (keyboardButtonActionRecyclerViewData[adapterPosition].editKeyboardButtonActionViewConstants) {
                EditKeyboardButtonActionViewConstants.LongClickData -> {
                  deleteLongClick()
                }
                EditKeyboardButtonActionViewConstants.MultipleShortClickData -> {
                  deleteShortClick(keyboardButtonActionRecyclerViewData.positionToShortClickIndex(adapterPosition))
                }
                else -> {
                  // No-op
                }
              }
            }
            keyboardButtonActionAddButton.setOnClickListener {
              when (keyboardButtonActionRecyclerViewData[adapterPosition].editKeyboardButtonActionViewConstants) {
                EditKeyboardButtonActionViewConstants.AddLongClickData -> {
                  addLongClick()
                }
                EditKeyboardButtonActionViewConstants.MultipleShortClickData,
                EditKeyboardButtonActionViewConstants.AddShortClickData -> {
                  addShortClick(keyboardButtonActionRecyclerViewData.positionToShortClickIndex(adapterPosition) + 1)
                }
                else -> {
                  // No-op
                }
              }
            }
          }
        }
      }

      override fun getItemCount(): Int = keyboardButtonActionRecyclerViewData.size

      override fun onBindViewHolder(
        holder: EditKeyboardButtonActionViewHolder, position: Int
      ) {
        holder.editKeyboardButtonActionView.run {
          keyboardButtonActionRecyclerViewData[position].run {
            keyboardButtonActionTypeTextView.text = typeTextViewText
            keyboardButtonActionDeleteButton.visibility = deleteButtonVisiblity
            keyboardButtonActionAddButton.visibility = addButtonVisibility
            keyboardCallableRecyclerView.adapter = KeyboardCallableAdapter(reactable, position)
          }
        }
      }

      private fun deleteLongClick() {
        EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
          (
              rows[rowIndex].buttons[buttonIndex]
                  as? RowBasedKeyboard.AbstractButton.LongClickableBuilder
              )?.longClick = null
        }
      }

      private fun deleteShortClick(index: Int) {
        EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
          (
              rows[rowIndex].buttons[buttonIndex]
                  as? RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.Builder
              )?.shortClicks?.removeAt(index)
        }
      }

      private fun addLongClick() {
        EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
          (
              rows[rowIndex].buttons[buttonIndex]
                  as? RowBasedKeyboard.AbstractButton.LongClickableBuilder
              )?.longClickReaction {}
        }
      }

      private fun addShortClick(index: Int) {
        EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
          (
              rows[rowIndex].buttons[buttonIndex]
                  as? RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.Builder
              )?.shortClicks?.add(index, RowBasedKeyboard.Reaction.Builder())
        }
      }
    }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_edit_button, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    val keyboardButtonTypeArrayAdapter =
      ArrayAdapter.createFromResource(
        activity!!,
        R.array.keyboard_button_type_values,
        R.layout.support_simple_spinner_dropdown_item
      )
    keyboard_button_type_spinner.run {
      fun updateKeyboardButtonType() =
        setSelection(
          keyboardButtonTypeArrayAdapter.getPosition(
            ViewModelProviders.of(activity!!)[EditorViewModel::class.java]
              .selectedButton.kclass?.simpleName
          )
        )

      adapter = keyboardButtonTypeArrayAdapter

      ViewModelProviders.of(activity!!)[EditorViewModel::class.java]
        .selectedRowAndButtonIndex.observe(
        this@EditButtonFragment,
        Observer { updateKeyboardButtonType() }
      )

      onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
          // No-op
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
          EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
            fun changeButtonTypeWhenUserSelect(
              buttonBuilderKClass: KClass<out RowBasedKeyboard.AbstractButton.Builder>
            ) {
              if (!buttonBuilderKClass.isInstance(rows[rowIndex].buttons[buttonIndex])) {
                rows[rowIndex].buttons[buttonIndex] =
                  buttonBuilderKClass.constructors.first { it.parameters.isEmpty() }.call()
              }
            }

            when (getItemAtPosition(position)) {
              "CustomLongClickButton" -> changeButtonTypeWhenUserSelect(
                RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder::class
              )
              "RepeaterButton" -> changeButtonTypeWhenUserSelect(
                RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Builder::class
              )
              "BaseButton" -> changeButtonTypeWhenUserSelect(
                RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.BaseButton.Builder::class
              )
              else -> throw NoWhenBranchMatchedException()
            }
          }
        }
      }
    }

    keyboard_button_action_recycler_view.adapter = keyboardButtonActionAdapter

    ViewModelProviders.of(activity!!)[EditorViewModel::class.java]
      .keyboard.observe(
        this@EditButtonFragment,
        Observer { keyboardButtonActionAdapter.notifyDataSetChanged() }
      )
  }

  private inner class KeyboardCallableAdapter(
    val reactable: RowBasedKeyboard.Reaction?,
    val position: Int
  ) : RecyclerView.Adapter<EditKeyboardCallableViewHolder>() {
    override fun onCreateViewHolder(
      parent: ViewGroup, viewType: Int
    ): EditKeyboardCallableViewHolder {
      return EditKeyboardCallableViewHolder(
        EditKeyboardCallableView(activity!!).apply {
          layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
          )
        }
      ).apply {
        editKeyboardCallableView.run {
          keyboardCallableDeleteButton.setOnClickListener {
            val callableIndex = adapterPosition - 1
            when (
              keyboardButtonActionRecyclerViewData[this@KeyboardCallableAdapter.position]
                .editKeyboardButtonActionViewConstants
            ) {
              EditKeyboardButtonActionViewConstants.AddLongClickData,
              EditKeyboardButtonActionViewConstants.AddShortClickData ->
                { /* No-op*/ }
              EditKeyboardButtonActionViewConstants.LongClickData ->
                deleteLongClickCallable(callableIndex)
              EditKeyboardButtonActionViewConstants.OnlyOneShortClickData ->
                deleteOnlyOneShortClickCallable(callableIndex)
              EditKeyboardButtonActionViewConstants.MultipleShortClickData ->
                deleteMultipleShortClickCallable(callableIndex)
            }
          }
          keyboardCallableAddButton.setOnClickListener {
            when (
              keyboardButtonActionRecyclerViewData[this@KeyboardCallableAdapter.position]
                .editKeyboardButtonActionViewConstants
            ) {
              EditKeyboardButtonActionViewConstants.AddLongClickData,
              EditKeyboardButtonActionViewConstants.LongClickData ->
                addLongClickCallable(adapterPosition)
              EditKeyboardButtonActionViewConstants.AddShortClickData,
              EditKeyboardButtonActionViewConstants.MultipleShortClickData ->
                addMultipleShortClickCallable(adapterPosition)
              EditKeyboardButtonActionViewConstants.OnlyOneShortClickData ->
                addOnlyOneShortClickCallable(adapterPosition)
            }
          }
        }
      }
    }

    override fun getItemCount(): Int {
      return reactable?.callables?.size?.plus(1 /* "add callable here" item */) ?: 0
    }

    override fun onBindViewHolder(
      holder: EditKeyboardCallableViewHolder, position: Int
    ) {
      holder.editKeyboardCallableView.run {
        when (position) {
          0 -> {
            keyboardCallableTypeTextView.text = "add callable here"
            keyboardCallableDeleteButton.visibility = View.INVISIBLE
            keyboardCallableAddButton.visibility = View.VISIBLE
          }
          else -> {
            keyboardCallableTypeTextView.text =
              with(reactable?.callables?.get(position - 1)) {
                when (this) {
                  is MetakeyboardLayout.Callable.Composee ->
                    "compose ${composerComposee.category.name}: " +
                        "\"${composerComposee.value}\", " +
                        "${composerComposee.languageComposer?.simpleName}"

                  is MetakeyboardLayout.Callable.Callee ->
                    StringBuilder("call ${callee.functionName}(").apply {
                      callee.arguments.forEachAndBetween(
                        { append(it) },
                        { _, _ -> append(", ") }
                      )
                      append(")")
                    }

                  else ->
                    "???"
                }
              }
            keyboardCallableDeleteButton.visibility = View.VISIBLE
            keyboardCallableAddButton.visibility = View.VISIBLE
          }
        }
      }
    }

    private fun deleteLongClickCallable(index: Int) {
      EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
        (
            rows[rowIndex].buttons[buttonIndex]
                as? RowBasedKeyboard.AbstractButton.LongClickableBuilder
            )?.longClick?.callables?.removeAt(index)
      }
    }

    private fun addLongClickCallable(index: Int) {
      showMakeCallableDialog { callable ->
        EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
          (
              rows[rowIndex].buttons[buttonIndex]
                  as? RowBasedKeyboard.AbstractButton.LongClickableBuilder
              )?.longClick?.callables?.add(index, callable)
        }
      }
    }

    private fun deleteOnlyOneShortClickCallable(index: Int) {
      EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
        (
            rows[rowIndex].buttons[buttonIndex]
                as? RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.Builder
            )?.shortClick?.callables?.removeAt(index)
      }
    }

    private fun addOnlyOneShortClickCallable(index: Int) {
      showMakeCallableDialog { callable ->
        EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
          (
              rows[rowIndex].buttons[buttonIndex]
                  as? RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.Builder
              )?.shortClick?.callables?.add(index, callable)
        }
      }
    }

    private fun deleteMultipleShortClickCallable(index: Int) {
      EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
        (
            rows[rowIndex].buttons[buttonIndex]
                as? RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.Builder
            )
          ?.shortClicks
          ?.get(keyboardButtonActionRecyclerViewData.positionToShortClickIndex(position))
          ?.callables
          ?.removeAt(index)
      }
    }

    private fun addMultipleShortClickCallable(index: Int) {
      showMakeCallableDialog { callable ->
        EditorViewModel.modifyKeyboard(this@EditButtonFragment) { rowIndex, buttonIndex ->
          (
              rows[rowIndex].buttons[buttonIndex]
                  as? RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.Builder
              )
            ?.shortClicks
            ?.get(keyboardButtonActionRecyclerViewData.positionToShortClickIndex(position))
            ?.callables
            ?.add(index, callable)
        }
      }
    }

    private fun showMakeCallableDialog(action: (callable: MetakeyboardLayout.Callable) -> Unit) {
      EditKeyboardCallableDetailView(activity!!).run {
        AlertDialog.Builder(activity!!)
          .setTitle("Make callable")
          .setView(this)
          .setNegativeButton("Cancel", null)
          .setPositiveButton("OK") { _, _ ->
            action(
              MetakeyboardLayout.Callable.Composee(
              PrimitiveComposer.Category.valueOf(composeCategorySpinner.selectedItem.toString()),
              composeValueEditText.text.toString(),
              languageComposerEditText.text.toString().let {
                try {
                  findLanguageComposer(it)
                } catch (exc: IllegalArgumentException) {
                  null
                }
              }
            ))
          }
          .show()
      }
    }
  }

  private enum class EditKeyboardButtonActionViewConstants(
    val typeTextViewText: CharSequence,
    val deleteButtonVisibility: Int,
    val addButtonVisibility: Int
  ) {
    AddLongClickData(
      "add long click action here",
      deleteButtonVisibility = View.INVISIBLE,
      addButtonVisibility = View.VISIBLE
    ),
    LongClickData(
      "long click action",
      deleteButtonVisibility = View.VISIBLE,
      addButtonVisibility = View.INVISIBLE
    ),
    AddShortClickData(
      "add short click action here",
      deleteButtonVisibility = View.INVISIBLE,
      addButtonVisibility = View.VISIBLE
    ),
    MultipleShortClickData(
      "short click action",
      deleteButtonVisibility = View.VISIBLE,
      addButtonVisibility = View.VISIBLE
    ),
    OnlyOneShortClickData(
      "short click action",
      deleteButtonVisibility = View.INVISIBLE,
      addButtonVisibility = View.INVISIBLE
    );
  }

  private class EditKeyboardButtonActionViewData(
    val editKeyboardButtonActionViewConstants: EditKeyboardButtonActionViewConstants,
    val reactable: RowBasedKeyboard.Reaction? = null
  ) {
    val typeTextViewText: CharSequence
      get() = editKeyboardButtonActionViewConstants.typeTextViewText
    val deleteButtonVisiblity: Int
      get() = editKeyboardButtonActionViewConstants.deleteButtonVisibility
    val addButtonVisibility: Int
      get() = editKeyboardButtonActionViewConstants.addButtonVisibility
  }

  private class EditKeyboardButtonActionViewHolder(
    val editKeyboardButtonActionView: EditKeyboardButtonActionView
  ) : RecyclerView.ViewHolder(editKeyboardButtonActionView)

  private class EditKeyboardCallableViewHolder(
    val editKeyboardCallableView: EditKeyboardCallableView
  ) : RecyclerView.ViewHolder(editKeyboardCallableView)
}
