package org.metakeyboard.android

import org.metakeyboard.core.MetakeyboardLayout
import org.metakeyboard.core.RowBasedKeyboard
import org.xmlpull.v1.XmlPullParserFactory
import java.io.OutputStream

/**
 * [RowBasedKeyboard] to XML document serializer.
 */
object XmlSerializer {
  private val serializer = XmlPullParserFactory.newInstance().newSerializer()

  /**
   * It serializes [RowBasedKeyboard] to XML document.
   *
   * @param [outputStream] to write the document.
   * @param [keyboard] being serialized.
   */
  fun serialize(outputStream: OutputStream, keyboard: RowBasedKeyboard) {
    serializer.run {
      setOutput(outputStream, "utf-8")
      startDocument("utf-8", null)
      setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true)
      startTag(null, "metakeyboard")
      attribute(null, "xmlns", "https://BugInMyHEAD.gitlab.io/metakeyboard/schema/v0_experimental/xsd")

      startTag(null, "row-based-keyboard-v0-experimental")
      attribute(null, "orientation", keyboard.orientation.name.toLowerCase())
      attribute(null, "name", "generated")
      aciton(keyboard.initialSetting, "initial-setting")
      keyboard.rows.forEach { keyboardRow(it) }
      endTag(null, "row-based-keyboard-v0-experimental")

      endTag(null, "metakeyboard")
      endDocument()
    }
  }

  private fun keyboardRow(keyboardRow: RowBasedKeyboard.KeyboardRow) {
    serializer.startTag(null, "keyboard-row")
    serializer.attribute(null, "weight", keyboardRow.weight.toString())
    keyboardRow.buttons.forEach {
      when (it) {
        is RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.BaseButton ->
          baseButton(it)
        is RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton ->
          repeaterButton(it)
        is RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton ->
          customLongClickButton(it)
        is RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.PopupButton ->
          popupButton(it)
      }
    }
    serializer.endTag(null, "keyboard-row")
  }

  private fun baseButton(
    baseButton: RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.BaseButton
  ) {
    serializer.startTag(null, "base-button")
    buttonCommon(baseButton)
    baseButton.shortClick.callables.forEach { callable(it) }
    serializer.endTag(null, "base-button")
  }

  private fun repeaterButton(
    repeaterButton: RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton
  ) {
    serializer.startTag(null, "repeater-button")
    serializer.attribute(null, "rate", repeaterButton.rate.name.toLowerCase())
    buttonCommon(repeaterButton)
    repeaterButton.longClick?.let { longClick(it) }
    shortClick(repeaterButton.shortClick)
    serializer.endTag(null, "repeater-button")
  }

  private fun customLongClickButton(
    customLongClickButton: RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton
  ) {
    serializer.startTag(null, "custom-long-click-button")
    serializer.attribute(null, "time-limit", customLongClickButton.hasTimeLimit.toString())
    serializer.attribute(null, "one-way", customLongClickButton.isOneWay.toString())
    buttonCommon(customLongClickButton)
    customLongClickButton.longClick?.let { longClick(it) }
    customLongClickButton.shortClicks.forEach { shortClick(it) }
    serializer.endTag(null, "custom-long-click-button")
  }

  private fun popupButton(
    popupButton: RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.PopupButton
  ) {
    serializer.startTag(null, "popup-button")
    buttonCommon(popupButton)

    serializer.endTag(null, "popup-button")
  }

  /**
   * Use it after serializing button specific attributes, and
   * before serializing nested tags.
   *
   * It includes serializing `imprint` tag.
   */
  private fun buttonCommon(button: RowBasedKeyboard.AbstractButton) {
    serializer.attribute(null, "weight", button.weight.toString())
    serializer.startTag(null, "imprint")
    button.imprints.forEach { buttonText(it) }
    serializer.endTag(null, "imprint")
  }

  private fun buttonText(buttonText: RowBasedKeyboard.ButtonText) {
    serializer.startTag(null, "button-text")
    buttonText.texts.forEach { buttonTextText(it) }
    serializer.endTag(null, "button-text")
  }

  private fun buttonTextText(buttonTextText: RowBasedKeyboard.ButtonText.Text) {
    serializer.startTag(null, "text")
    serializer.attribute(null, "text", buttonTextText.text)
    serializer.endTag(null, "text")
  }

  private fun longClick(longClick: RowBasedKeyboard.Reaction) =
    reaction(longClick, "long-click")

  private fun shortClick(shortClick: RowBasedKeyboard.Reaction) =
    reaction(shortClick, "short-click")

  private fun reaction(reaction: RowBasedKeyboard.Reaction, tagName: String) {
    aciton(reaction, tagName)
  }

  private fun aciton(action: MetakeyboardLayout.Action, tagName: String) {
    serializer.startTag(null, tagName)
    action.callables.forEach { callable(it) }
    serializer.endTag(null, tagName)
  }

  private fun callable(callable: MetakeyboardLayout.Callable) {
    with(serializer) {
      when (callable) {
        is MetakeyboardLayout.Callable.Callee -> {
          startTag(null, "call")
          attribute(null, "function", callable.callee.functionName)
          callable.callee.arguments.forEach {
            startTag(null, "argument")
            attribute(null, "type", it.type.name.toLowerCase())
            attribute(null, "value", it.value)
            endTag(null, "argument")
          }
          endTag(null, "call")
        }
        is MetakeyboardLayout.Callable.Composee -> {
          startTag(null, "compose")
          attribute(null, "category", callable.composerComposee.category.name.toLowerCase())
          attribute(null, "value", callable.composerComposee.value)
          callable.composerComposee.languageComposer?.simpleName?.let { attribute(null, "composer", it) }
          callable.composerComposee.print?.let { attribute(null, "print", it) }
          endTag(null, "compose")
        }
      }
    }
  }
}