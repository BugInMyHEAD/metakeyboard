package org.metakeyboard.android

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_keyboard_item.view.*

class KeyboardItemView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {
  init {
    inflate(context, R.layout.view_keyboard_item, this)
  }

  val keyboardItemTextView = keyboard_item_text_view!!
}