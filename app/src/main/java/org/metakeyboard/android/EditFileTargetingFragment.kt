package org.metakeyboard.android

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_edit_file_targeting.*
import org.metakeyboard.core.rowBasedKeyboard

class EditFileTargetingFragment : Fragment() {
  private val editKeyboardFragment = EditKeyboardFragment()

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_edit_file_targeting, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    empty_keyboard_button.setOnClickListener {
      ViewModelProviders.of(activity!!)[EditorViewModel::class.java]
        .keyboard.value = rowBasedKeyboard {}

      activity
        ?.supportFragmentManager
        ?.beginTransaction()
        ?.hide(this)
        ?.add(R.id.activity_editor_root, editKeyboardFragment)
        ?.addToBackStack(null)
        ?.commit()
    }

    // TODO: find where to insert notifyDataSetChanged()
    keyboard_recycler_view.adapter = object : RecyclerView.Adapter<KeyboardViewHolder>() {
      // TODO: handle exception when getKeyboards() returns null
      val keyboards = MetakeyboardApplication.getKeyboards()!!

      override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KeyboardViewHolder {
        return KeyboardViewHolder(KeyboardItemView(activity!!)).apply {
          keyboardItemView.run {
            layoutParams = ViewGroup.LayoutParams(
              ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
            )
            setOnClickListener {
              val inputStream =
                activity!!.contentResolver.openInputStream(keyboards[adapterPosition].uri)!!
              ViewModelProviders.of(activity!!)[EditorViewModel::class.java].run {
                keyboard.value = XmlDeserializer.rowBasedKeyboard(inputStream)
                fileName = keyboards[adapterPosition].name ?: ""
              }
              inputStream.close()
              activity
                ?.supportFragmentManager
                ?.beginTransaction()
                ?.hide(this@EditFileTargetingFragment)
                ?.add(R.id.activity_editor_root, editKeyboardFragment)
                ?.addToBackStack(null)
                ?.commit()
            }
          }
        }
      }

      override fun getItemCount(): Int = keyboards.size

      override fun onBindViewHolder(holder: KeyboardViewHolder, position: Int) {
        holder.keyboardItemView.keyboardItemTextView.text = keyboards[position].name
      }
    }
  }

  class KeyboardViewHolder(
    val keyboardItemView: KeyboardItemView
  ) : RecyclerView.ViewHolder(keyboardItemView)
}