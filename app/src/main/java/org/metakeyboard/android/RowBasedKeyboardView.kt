package org.metakeyboard.android

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import android.widget.Toast
import org.metakeyboard.core.RowBasedKeyboard
import org.metakeyboard.core.rowBasedKeyboard
import java.io.FileNotFoundException
import kotlin.math.min
import kotlin.math.roundToInt

open class RowBasedKeyboardView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {
  internal var getKeyboard: () -> RowBasedKeyboard = { rowBasedKeyboard {} }
  internal var keyboardModel: RowBasedKeyboard.Model? = null
    private set

  private var backgroundImage: Bitmap? = null
  private val backgroundPaint = Paint().apply { style = Paint.Style.FILL_AND_STROKE; }
  private val buttonPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply { style = Paint.Style.FILL_AND_STROKE; }
  private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply { style = Paint.Style.FILL_AND_STROKE; }

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    val measureSpecKeyboardHeight =
      MeasureSpec.getSize(heightMeasureSpec)
    val settingsKeyboardHeight =
      MetakeyboardApplication.getSettingsInt(
        "keyboard_height",
        R.integer.pref_keyboard_height_default
      )
    @SuppressLint("SwitchIntDef")
    val actualKeyboardHeight = when (MeasureSpec.getMode(heightMeasureSpec)) {
      MeasureSpec.AT_MOST -> min(measureSpecKeyboardHeight, settingsKeyboardHeight)
      MeasureSpec.EXACTLY -> measureSpecKeyboardHeight
      else -> settingsKeyboardHeight
    }
    setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), actualKeyboardHeight)
  }

  override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
    val viewWidth = right - left
    val viewHeight = bottom - top
    val viewRatio = viewWidth.toDouble() / viewHeight.toDouble()

    try {
      backgroundImage = null
      val uri = Uri.parse(MetakeyboardApplication.getSettingsString("keyboard_background_image", ""))
      if (uri.toString().isNotEmpty()) {
        val source = BitmapFactory.decodeStream(context.contentResolver.openInputStream(uri))
        val sourceRatio = source.width.toDouble() / source.height.toDouble()
        val scaled =
          when (
            MetakeyboardApplication.getSettingsString(
              "keyboard_background_image_policy",
              R.string.pref_keyboard_background_image_policy_default
            )
            ) {
            "stretch" -> {
              Bitmap.createScaledBitmap(source, viewWidth, viewHeight, true)
            }
            // crop(default) or else
            else -> {
              if (sourceRatio < viewRatio) Bitmap.createScaledBitmap(
                source, viewWidth, (viewWidth / sourceRatio).roundToInt(), true
              ) else Bitmap.createScaledBitmap(
                source, (viewHeight * sourceRatio).roundToInt(), viewHeight, true
              )
            }
          }
        backgroundImage = Bitmap.createBitmap(
          scaled,
          if (scaled.width <= viewWidth) 0 else (scaled.width - viewWidth) / 2,
          if (scaled.height <= viewHeight) 0 else (scaled.height - viewHeight) / 2,
          viewWidth,
          viewHeight
        )
      }
    } catch (exc: FileNotFoundException) {
      Toast.makeText(context, "파일이 삭제되었거나 훼손되었습니다. 이미지 파일을 다시 골라주세요", Toast.LENGTH_SHORT).show()
    } catch (exc: SecurityException) {
      Toast.makeText(context, "파일이 삭제되었거나 훼손되었습니다. 이미지 파일을 다시 골라주세요", Toast.LENGTH_SHORT).show()
    }

    keyboardModel = getKeyboard().Model(viewWidth.toFloat(), viewHeight.toFloat())
  }

  override fun onDraw(canvas: Canvas) {
    val alphaMask =
      if (MetakeyboardApplication.getSettingsString("keyboard_background_image", "").isEmpty()) Color.BLACK
      else Color.TRANSPARENT
    backgroundPaint.color =
      MetakeyboardApplication.getSettingsInt(
        "keyboard_background_color",
        R.color.colorKeyboardBackground
      ) or alphaMask
    buttonPaint.color =
      MetakeyboardApplication.getSettingsInt(
        "keyboard_button_color",
        R.color.colorKeyboardButton
      ) or alphaMask
    textPaint.color =
      MetakeyboardApplication.getSettingsInt(
        "keyboard_text_color",
        R.color.colorKeyboardText
      ) or alphaMask
    textPaint.textSize = 50F
    textPaint.textAlign = Paint.Align.CENTER
    val textHeight = textPaint.descent() - textPaint.ascent()
    val textOffset = textHeight / 2 - textPaint.descent()
    val gap = MetakeyboardApplication.getSettingsInt(
      "keyboard_gap_size",
      R.integer.pref_button_gap_default
    )

    backgroundImage?.let { canvas.drawBitmap(it, 0F, 0F, null) }

    if (/*keyboard.hasButton()*/true) {
      var lastRowBorderline = 0F
      keyboardModel?.rowModelBorderlines?.forEachIndexed { rowIndex, rmb ->
        canvas.drawRect(0F, lastRowBorderline, width.toFloat(), lastRowBorderline + gap, backgroundPaint)

        var lastButtonBorderline = 0F
        rmb.model.buttonBorderlines.forEachIndexed { buttonIndex, buttonBorderline ->
          canvas.drawRect(lastButtonBorderline, lastRowBorderline + gap, lastButtonBorderline + gap, rmb.borderline - gap, backgroundPaint)

          canvas.save()
          canvas.clipRect(lastButtonBorderline + gap, lastRowBorderline + gap, buttonBorderline - gap, rmb.borderline - gap)
          canvas.drawPaint(buttonPaint)
          getKeyboard().rows[rowIndex].buttons[buttonIndex].imprints.forEach {
            val text = it.texts.fold("") { acc, text -> acc + text.text }
            canvas.drawText(text, (lastButtonBorderline + buttonBorderline) / 2, (lastRowBorderline + rmb.borderline) / 2 + textOffset, textPaint)
          }
          canvas.restore()

          canvas.drawRect(buttonBorderline - gap, lastRowBorderline + gap, buttonBorderline, rmb.borderline - gap, backgroundPaint)

          lastButtonBorderline = buttonBorderline
        }

        canvas.drawRect(0F, rmb.borderline - gap, width.toFloat(), rmb.borderline, backgroundPaint)
        lastRowBorderline = rmb.borderline
      }
    } else {
      canvas.drawColor(backgroundPaint.color)
    }
  }
}