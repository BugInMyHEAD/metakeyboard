package org.metakeyboard.android

import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.ViewModelProviders

class EditorActivity : AppCompatActivity() {
  private val editFileTargetingFragment = EditFileTargetingFragment()
  private lateinit var fileNameEditText: EditText
  private lateinit var savingAlertDialog: AlertDialog

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_editor)

    fileNameEditText = EditText(this)
    savingAlertDialog =
      AlertDialog.Builder(this)
        .setTitle("Save As")
        .setView(fileNameEditText)
        .setNegativeButton("Cancel", null)
        .setPositiveButton("OK") { _, _ ->
          val outputStream = contentResolver.openOutputStream(
            DocumentFile
              .fromTreeUri(
                this,
                Uri.parse(
                  MetakeyboardApplication.getSettingsString("load_keyboard_layout_directory", "")
                )
              )!!
              .createFile("text/xml", fileNameEditText.text.toString())!!
              .uri
          )!!
          XmlSerializer.serialize(
            outputStream,
            ViewModelProviders.of(this)[EditorViewModel::class.java].keyboard.value!!
          )
          outputStream.close()
          ViewModelProviders.of(this@EditorActivity)[EditorViewModel::class.java]
            .fileName = fileNameEditText.text.toString()
        }
        .create()

    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    supportFragmentManager.run {
      if (backStackEntryCount == 0) {
        beginTransaction()
          .replace(R.id.activity_editor_root, editFileTargetingFragment)
          .commit()
      }
    }
  }

  override fun onSupportNavigateUp(): Boolean {
    return if (supportFragmentManager.popBackStackImmediate()) true
    else super.onSupportNavigateUp()
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.editor_activity_action_buttons, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    return when (item?.itemId) {
      R.id.action_save -> {
        fileNameEditText.text.run {
          clear()
          append(
            ViewModelProviders.of(this@EditorActivity)[EditorViewModel::class.java]
              .fileName
          )
        }
        savingAlertDialog.show()
        true
      }
      else -> {
        super.onOptionsItemSelected(item)
      }
    }
  }
}
