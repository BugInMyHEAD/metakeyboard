package org.metakeyboard.android

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_keyboard_callable_detail.view.*

class EditKeyboardCallableDetailView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {
  init {
    inflate(context, R.layout.view_keyboard_callable_detail, this)
  }

  val keyboardCallableTypeSpinner = keyboard_callable_type_spinner
  val keyboardCallableTypeSpecificView = keyboard_callable_type_specific_view

  val languageComposerEditText = language_composer_edit_text
  val composeCategorySpinner = compose_category_spinner
  val composeValueEditText = compose_value_edit_text

  val functionNameEditText = function_name_edit_text
}