package org.metakeyboard.android

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_edit_keyboard_button_action.view.*

class EditKeyboardButtonActionView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {
  init {
    inflate(context, R.layout.view_edit_keyboard_button_action, this)
  }

  val keyboardButtonActionTypeTextView = keyboard_button_action_type_text_view!!
  val keyboardButtonActionDeleteButton = keyboard_button_action_delete_button!!
  val keyboardButtonActionAddButton = keyboard_button_action_add_button!!
  val keyboardCallableRecyclerView = keyboard_callable_recycler_view!!
}