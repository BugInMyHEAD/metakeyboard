package org.metakeyboard.android

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_edit_keyboard.*
import org.metakeyboard.core.RowBasedKeyboard
import org.metakeyboard.core.rowBasedKeyboard

class EditKeyboardFragment : Fragment() {
  private val editButtonFragment = EditButtonFragment()

  private val editorViewModel
    get() = ViewModelProviders.of(activity!!)[EditorViewModel::class.java]

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_edit_keyboard, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)

    editorViewModel.run {
      if (keyboard.value == null) {
        keyboard.value = rowBasedKeyboard {}
      }
      keyboard.observe(
        this@EditKeyboardFragment,
        Observer {
          // Update keyboard model
          keyboard_view.requestLayout()
        }
      )
      selectedRowAndButtonIndex.observe(
        this@EditKeyboardFragment,
        Observer {
          keyboard_view.selectedRowAndButtonIndex = it
          button_text_view.text =
            it.runCatching {
              "row: ${rowIndex}" +
                  runCatching {
                    ", button: ${buttonIndex}, " +
                        keyboard.value!!.rows[rowIndex].buttons[buttonIndex]::class.simpleName
                  }.getOrDefault("")
            }.getOrDefault("")
        }
      )
    }

    keyboard_view.let {
      it.getKeyboard = { editorViewModel.keyboard.value ?: rowBasedKeyboard {} }
      it.setOnTouchListener { _, event ->
        it.keyboardModel?.run {
          editorViewModel.newRationalRowAndButtonIndex(getRowAndButtonIndex(event.x, event.y))
        }
        true
      }
    }

    left_button.setOnClickListener {
      editorViewModel.moveToRationalRowAndButtonIndex(0, -1)
    }
    down_button.setOnClickListener {
      editorViewModel.moveToRationalRowAndButtonIndex(1, 0)
    }
    right_button.setOnClickListener {
      editorViewModel.moveToRationalRowAndButtonIndex(0, 1)
    }
    up_button.setOnClickListener {
      editorViewModel.moveToRationalRowAndButtonIndex(-1, 0)
    }
    delete_button_button.setOnClickListener {
      editorViewModel.selectedButton ?: return@setOnClickListener

      EditorViewModel.modifyKeyboard(this) { rowIndex, buttonIndex ->
        rows[rowIndex].buttons.removeAt(buttonIndex)
      }
    }
    delete_row_button.setOnClickListener {
      editorViewModel.selectedRow ?: return@setOnClickListener

      EditorViewModel.modifyKeyboard(this) { rowIndex, _ ->
        rows.removeAt(rowIndex)
      }
    }
    edit_button_button.setOnClickListener {
      editorViewModel.selectedButton ?: return@setOnClickListener

      activity
        ?.supportFragmentManager
        ?.beginTransaction()
        ?.hide(this)
        ?.add(R.id.activity_editor_root, editButtonFragment)
        ?.addToBackStack(null)
        ?.commit()
    }
    add_button_button.setOnClickListener {
      editorViewModel.selectedRow ?: return@setOnClickListener

      EditorViewModel.modifyKeyboard(this) { rowIndex, buttonIndex ->
        rows[rowIndex].buttons.add(
          buttonIndex + 1,
          RowBasedKeyboard.AbstractButton.AbstractSwitcherButton.CustomLongClickButton.Builder()
        )
      }
    }
    add_row_button.setOnClickListener {
      EditorViewModel.modifyKeyboard(this) { rowIndex, _ ->
        rows.add(rowIndex + 1, RowBasedKeyboard.KeyboardRow.Builder())
      }
    }

    editorViewModel.newRationalRowAndButtonIndex(0, 0)
  }
}