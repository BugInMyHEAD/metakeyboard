package org.metakeyboard.android

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.view.Gravity
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.documentfile.provider.DocumentFile
import androidx.preference.PreferenceManager
import org.metakeyboard.core.MetakeyboardModel
import org.metakeyboard.core.RowBasedKeyboard
import org.metakeyboard.core.rowBasedKeyboard
import java.io.FileNotFoundException

class MetakeyboardApplication : Application(),
  SharedPreferences.OnSharedPreferenceChangeListener {
  companion object {
    var keyboard: RowBasedKeyboard = rowBasedKeyboard {}

    private lateinit var metakeyboardApplication: MetakeyboardApplication

    @SuppressLint("InlinedApi")
    fun requestOverlayPermission(context: Context): Boolean {
      return (
          Build.VERSION.SDK_INT < Build.VERSION_CODES.M
              || Settings.canDrawOverlays(context)
          ).also {
        if (!it) context.startActivity(
          Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:${context.packageName}"))
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK),
          null
        )
      }
    }

    private fun loadKeyboard() =
      try {
        val uri = Uri.parse(getSettingsString("load_keyboard_layout", ""))
        keyboard = XmlDeserializer.rowBasedKeyboard(
          metakeyboardApplication.contentResolver.openInputStream(uri)!!
        )
      } catch (exc: FileNotFoundException) {
        MetakeyboardModel.showKeyboardListCallback()
//      if (PreferenceManager.getDefaultSharedPreferences(context).getString("load_keyboard_layout", "") == "")
//        Toast.makeText(context, "정상적인 앱 사용을 위해서는 레이아웃 파일을 설정해 주셔야 합니다", Toast.LENGTH_LONG).show()
//      else
//        Toast.makeText(context, "파일이 삭제되었거나 훼손되었습니다. 레이아웃 파일을 다시 골라주세요", Toast.LENGTH_LONG).show()
      } catch (exc: IllegalArgumentException) {
        MetakeyboardModel.showKeyboardListCallback()
      }

    fun onSharedPreferenceChanged(key: String, keyboardView: RowBasedKeyboardView) {
      if (key == "load_keyboard_layout") {
        loadKeyboard()
      }

      when (key) {
        "keyboard_height",
        "master_row_width",
        "load_keyboard_layout",
        "keyboard_background_image",
        "keyboard_background_image_policy" -> {
          // https://developer.android.com/training/custom-views/create-view#addprop
          keyboardView.requestLayout()
          keyboardView.invalidate()
        }
      }
    }

    fun getKeyboards(): List<DocumentFile>? {
      val treeUri = Uri.parse(
        getSettingsString("load_keyboard_layout_directory", "")
      )
      return try {
        DocumentFile
          .fromTreeUri(metakeyboardApplication, treeUri)!!
          .listFiles()
          .filter { it.name?.endsWith(".xml", true) ?: true }
      } catch (exc: IllegalArgumentException) {
        metakeyboardApplication.startActivity(
          Intent(metakeyboardApplication, SettingsActivity::class.java)
            .putExtra("setting", "select_directory")
            // To avoid "android.util.AndroidRuntimeException:
            // Calling startActivity() from outside of an Activity  context requires
            // the FLAG_ACTIVITY_NEW_TASK flag. Is this really what you want?"
            .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK }
        )
        Toast.makeText(
          metakeyboardApplication, "키보드 선택을 하기 위해서는 폴더 선택이 필요합니다.", Toast.LENGTH_SHORT
        ).show()
        null
      }
    }

    fun getSettingsString(key: String, defValId: Int) =
      PreferenceManager
        .getDefaultSharedPreferences(metakeyboardApplication)
        .getString(key, metakeyboardApplication.resources.getString(defValId))!!

    fun getSettingsString(key: String, defVal: String) =
      PreferenceManager
        .getDefaultSharedPreferences(metakeyboardApplication)
        .getString(key, defVal)!!

    fun getSettingsInt(key: String, defValId: Int) =
      PreferenceManager
        .getDefaultSharedPreferences(metakeyboardApplication)
        .getInt(key, metakeyboardApplication.resources.getInteger(defValId))
  }

  override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
    onSharedPreferenceChanged(key, keyboardView)
  }

  private lateinit var keyboardView: RowBasedKeyboardView//MetakeyboardView
  private lateinit var keyboardBorderAdjusterLayout: KeyboardBorderAdjusterLayout

  override fun onCreate() {
    super.onCreate()
    // To avoid "android.content.res.Resources$NotFoundException: Resource ID #0x0"
    setTheme(R.style.AppTheme)

    metakeyboardApplication = this

    PreferenceManager
      .getDefaultSharedPreferences(this)
      .registerOnSharedPreferenceChangeListener(this)

    loadKeyboard()
    keyboardView = RowBasedKeyboardView(this).apply {
      getKeyboard = { keyboard }
    }
    keyboardBorderAdjusterLayout = KeyboardBorderAdjusterLayout(this).apply {
      removeViewFromWindow = {
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(this)
      }
      orientation = LinearLayout.VERTICAL
      gravity = Gravity.BOTTOM

      addView(KeyboardHeightControlBar(this@MetakeyboardApplication))
      addView(keyboardView)
    }

    MetakeyboardModel.openAppCallback = { args: List<String> ->
      try {
        startActivity(
          packageManager
            .apply { getApplicationInfo(args[0], PackageManager.GET_META_DATA) }
            .getLaunchIntentForPackage(args[0])
        )
      } catch (exc: PackageManager.NameNotFoundException) {
        startActivity(
          Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${args[0]}"))
            .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK }
        )
      }
    }
    MetakeyboardModel.openSettingsCallback = {
      startActivity(
        Intent(this, SettingsActivity::class.java)
          .apply { flags = Intent.FLAG_ACTIVITY_NEW_TASK }
      )
    }
    MetakeyboardModel.resizeCallback = {
      val windowType =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
          WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        else
          WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
      val layoutParams = WindowManager.LayoutParams(windowType).apply {
        format = PixelFormat.TRANSLUCENT
      }
      if (requestOverlayPermission(this)) {
        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).addView(keyboardBorderAdjusterLayout, layoutParams)
      }
    }
    MetakeyboardModel.showKeyboardListCallback = showKeyboardListCallback@{
      val fileList = getKeyboards() ?: return@showKeyboardListCallback
      val fileNameList = fileList.map { it.name }.toTypedArray()
      val keyboardListDialog =
        AlertDialog.Builder(this)
          .setTitle("Choose keyboard")
          .setItems(fileNameList) { _, which ->
            // Force to reload the same keyboard layout file of which SharedPreference isn't aware.
            // See https://developer.android.com/guide/topics/ui/settings/use-saved-values#listen_for_changes_to_preference_values
            if (getSettingsString("load_keyboard_layout", "") == fileList[which].uri.toString()) {
              loadKeyboard()
            } else {
              PreferenceManager
                .getDefaultSharedPreferences(this)
                .edit()
                .putString("load_keyboard_layout", fileList[which].uri.toString())
                .apply()
            }
          }
          .create()
      keyboardListDialog.window!!.setType(
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
          WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
          WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
        }
      )
      if (requestOverlayPermission(this)) {
        keyboardListDialog.show()
      }
    }
  }
}