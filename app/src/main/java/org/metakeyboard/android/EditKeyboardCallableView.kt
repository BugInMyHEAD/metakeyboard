package org.metakeyboard.android

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.view_edit_keyboard_callable.view.*

class EditKeyboardCallableView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyleAttr, defStyleRes) {
  init {
    inflate(context, R.layout.view_edit_keyboard_callable, this)
  }

  val keyboardCallableTypeTextView = keyboard_callable_type_text_view!!
  val keyboardCallableDeleteButton = keyboard_callable_delete_button!!
  val keyboardCallableAddButton = keyboard_callable_add_button!!
}