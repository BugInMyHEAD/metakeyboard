package org.metakeyboard.android

import android.content.SharedPreferences
import android.inputmethodservice.InputMethodService
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.preference.PreferenceManager
import org.metakeyboard.core.MetakeyboardLayout
import org.metakeyboard.core.MetakeyboardModel
import org.metakeyboard.core.RowBasedKeyboard

class MetakeyboardService : InputMethodService(),
    SharedPreferences.OnSharedPreferenceChangeListener {
  private lateinit var keyboardView: RowBasedKeyboardView

  private val fastClickTimer = Handler()
  private val fastClickRunnable = Runnable {
    keyboardView.keyboardModel?.onTime(MetakeyboardLayout.ModelDelegate.TimerEvent.SLOW_CLICK)
  }

  private var slowRepeatCount = 0
  private val longClickTimer = Handler()
  private val longClickRunnable = Runnable {
    slowRepeatCount = 0
    repeat(MetakeyboardLayout.ModelDelegate.TimerEvent.LONG_CLICK)
  }
  private val repeatRunnable = Runnable {
    repeat(MetakeyboardLayout.ModelDelegate.TimerEvent.REPEAT)
  }

  override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
    MetakeyboardApplication.onSharedPreferenceChanged(key, keyboardView)
  }

  override fun onCreate() {
    super.onCreate()

    PreferenceManager
        .getDefaultSharedPreferences(this)
        .registerOnSharedPreferenceChangeListener(this)

    keyboardView = RowBasedKeyboardView(this).also {
      it.getKeyboard = { MetakeyboardApplication.keyboard }
      it.setOnTouchListener { _, event ->
        MetakeyboardModel.integrationComposer.lackOfCommittedTextEventHandler = {
          val ret = currentInputConnection.getTextBeforeCursor(1, 0)
          currentInputConnection.deleteSurroundingText(1, 0)
          ret
        }

        when (event.action) {
          MotionEvent.ACTION_DOWN -> {
            longClickTimer.postDelayed(
              longClickRunnable,
              MetakeyboardApplication.getSettingsString(
                "long_click", R.string.pref_long_click_default
              ).toLong()
            )
            it.keyboardModel?.onTouch(event.x, event.y, MetakeyboardLayout.ModelDelegate.TouchAction.DOWN)
          }
          MotionEvent.ACTION_MOVE -> {
            it.keyboardModel?.onTouch(event.x, event.y, MetakeyboardLayout.ModelDelegate.TouchAction.MOVE)
          }
          MotionEvent.ACTION_UP -> {
            longClickTimer.removeCallbacksAndMessages(null)
            fastClickTimer.removeCallbacksAndMessages(null)
            fastClickTimer.postDelayed(
              fastClickRunnable, MetakeyboardApplication.getSettingsString(
                "fast_click", R.string.pref_fast_click_default
              ).toLong()
            )
            it.keyboardModel?.onTouch(event.x, event.y, MetakeyboardLayout.ModelDelegate.TouchAction.UP)
            updateText()
          }
        }

//        if (it.keyboardModel.popupModel == null) {
//
//        } else {
//
//        }

        true
      }
    }
  }

  override fun onDestroy() {
    PreferenceManager
        .getDefaultSharedPreferences(this)
        .unregisterOnSharedPreferenceChangeListener(this)

    super.onDestroy()
  }

  override fun onCreateInputView(): View {
    super.onCreateInputView()
    return keyboardView
  }

  override fun onStartInput(attribute: EditorInfo?, restarting: Boolean) {
    resetInputMethodEditor()
  }

  override fun onUpdateSelection(
      oldSelStart: Int, oldSelEnd: Int, newSelStart: Int, newSelEnd: Int,
      candidatesStart: Int, candidatesEnd: Int
  ) {
    if (oldSelStart == oldSelEnd && oldSelEnd == candidatesEnd) {
      currentInputConnection.finishComposingText()
      resetInputMethodEditor()
    }
    super.onUpdateSelection(
      oldSelStart, oldSelEnd, newSelStart, newSelEnd,
      candidatesStart, candidatesEnd
    )
  }

  private fun resetInputMethodEditor() {
    MetakeyboardModel.integrationComposer.clearCommitted()
    MetakeyboardModel.integrationComposer.clearUncommitted()
    keyboardView.keyboardModel?.resetClickCounter()
  }

  private fun updateText() {
    currentInputConnection.apply {
      commitText(MetakeyboardModel.integrationComposer.pullCommitted(), 1)
      setComposingText(MetakeyboardModel.integrationComposer.pullUncommitted(), 1)
    }
    MetakeyboardModel.integrationComposer.clearCommitted()
  }

  private fun repeat(timerEvent: MetakeyboardLayout.ModelDelegate.TimerEvent) {
    keyboardView.keyboardModel?.onTime(timerEvent)
    keyboardView.keyboardModel?.rate?.let {
      val repeaterRateFast =
        MetakeyboardApplication.getSettingsString(
          "repeater_rate_fast", R.string.pref_repeater_rate_fast_default
        ).toLong()
      val repeaterRateSlow =
        MetakeyboardApplication.getSettingsString(
          "repeater_rate_slow", R.string.pref_repeater_rate_slow_default
        ).toLong()
      val repeaterAccelerateCount =
        MetakeyboardApplication.getSettingsString(
          "repeater_accelerate_count", R.string.pref_repeater_accelerate_count_default
        ).toInt()
      longClickTimer.postDelayed(
        repeatRunnable,
        when (it) {
          RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Rate.FAST ->
            repeaterRateFast
          RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Rate.SLOW ->
            repeaterRateSlow
          RowBasedKeyboard.AbstractButton.AbstractNonSwitcherButton.RepeaterButton.Rate.ACCELERATE ->
            if (slowRepeatCount < repeaterAccelerateCount) {
              slowRepeatCount++
              repeaterRateSlow
            } else {
              repeaterRateFast
            }
        }
      )
    }
    updateText()
  }
}